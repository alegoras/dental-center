<?php

	class CerrarSesion extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$this->load->library('session');
		}

		public function Cerrar()
		{
			$this->session->sess_destroy();
			echo '<meta http-equiv="refresh" content="1;url=https://pruebasal.000webhostapp.com/">';
		}
	}
?>