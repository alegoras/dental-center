<?php
	/**
	* Controlador para perfil de usuario
	*/
	class Perfil extends CI_Controller
	{
		
		function __construct()
		{
			parent::__construct();
			$this->load->library('session');
			IF (! $this->session->has_userdata("user")) {
				echo '<meta http-equiv="refresh" content="0;url=https://pruebasal.000webhostapp.com/">';
			} else {
				$this->load->model("Primero");
			}
		}

		public function Ver( $idUser = false ){
			//Validación para que un usuario tipo "Cliente" No pueda insertar un nuevo usuario
			if (($idUser == false) && ($_SESSION["user"]->Tipo_Usuario == 3)) {
				echo '<meta http-equiv="refresh" content="0;url='. base_url() .'Admin/Inicio">';
			} else {
				//Validación para que un usuario tipo "Cliente" no pueda ver otros perfiles
				if (($idUser != $_SESSION["user"]->ID_Usuario) && ($_SESSION["user"]->Tipo_Usuario == 3)) {
					echo '<meta http-equiv="refresh" content="0;url='. base_url() .'Admin/Inicio">';
				} else{
					//var_dump($_SESSION["user"]);
					//---> Obteniendo todos los datos de la sesión
					$usuario = $this->Primero->get_perfil($_SESSION["user"]->Email_Usuario);
					$usuResult = $usuario->result();

					//---> Obteniendo la descripcion del Tipo de Usuario.
					$tipoUsuario = $this->Primero->get_tipousu($_SESSION["user"]->Tipo_Usuario);

					//---> Colocando los datos genericos y del usuario en un arreglo
					$dataSession = array(
						'tituloVista' => 'Perfil',
						'titulo1' => 'Dental',
						'titulo2' => 'Admin',
						'idperfil' => $_SESSION["user"]->ID_Usuario,
						'tipouser' => $tipoUsuario->result(),
						'usuario' => $usuResult);

					if (!$idUser){
						//echo "Qchau";
						$idperfil = "";
						$tipouser = "";
						$usuResult ="";
					} else {
						//---> Obteniendo todos los datos del usuario a modificar
						//$usuario = $this->Primero->get_perfil($_SESSION["user"]->Email_Usuario);
						//var_dump($usuario->result());
						$usuario = $this->Primero->get_user(array('ID_Usuario' => $idUser ));
						$usuResult = $usuario->result();
						//var_dump($usuResult);

						//---> Obteniendo la descripcion del Tipo de Usuario.
						$tipoUsuario = $this->Primero->get_tipousu($usuResult[0]->Tipo_Usuario);

						$idperfil = $idUser;
						$tipouser = $tipoUsuario->result();
					}

					
					//Validación para que un usuario tipo Doctor, solo pueda ver Clientes
					if ((($usuResult[0]->Tipo_Usuario == 2) || ($usuResult[0]->Tipo_Usuario == 1)) && (($_SESSION["user"]->Tipo_Usuario == 2) && ($idUser != $_SESSION["user"]->ID_Usuario))) {
							echo '<meta http-equiv="refresh" content="0;url='. base_url() .'Admin/Inicio">';
					} else{
						//---> Extrayendo datos de interrogatorio asociado
						$idExp = $this->Primero->get_Expediente($idperfil)->result();
						$dataInter = $this->Primero->get_Interrogatorio($idExp[0]->Expediente_Usuario)->result();

						//---> Colocando los datos genericos y del usuario a modificar en un arreglo
						if ($dataInter[0]->ID_Interr != '') {
							$AlergiaMedica = $dataInter[0]->AlergiaMedica_Interr;
							$DescripAlerMed = $dataInter[0]->DescripAlerMed_Interr;
							$Anestesia = $dataInter[0]->Anestesia_Interr;
							$ProblemAnestesia = $dataInter[0]->ProblemAnestesia_Interr;
							$DescripAnes = $dataInter[0]->DescripAnes_Interr;
							$TomaMedicamento = $dataInter[0]->TomaMedicamento_Interr;
							$DescripMed = $dataInter[0]->DescripMed_Interr;
							$Adicciones = $dataInter[0]->Adicciones_Interr;
							$CualesAdicc = $dataInter[0]->CualesAdicc_Interr;
							$Fumador = $dataInter[0]->Fumador_Interr;
							$BebAlcohol = $dataInter[0]->BebAlcohol_Interr;
							$Embarazada = $dataInter[0]->Embarazada_Interr;
							$MesesGesta = $dataInter[0]->MesesGesta_Interr;
							$ProblCPeriodo = $dataInter[0]->ProblCPeriodo_Interr;
							$OtraEnferm = $dataInter[0]->OtraEnferm_Interr;
						}

						$datosAModificar = array(
							//---> Variables de cuenta
							'tituloVista' => 'Perfil',
							'titulo1' => 'Dental',
							'titulo2' => 'Admin',
							'idperfil' => $idperfil,
							'tipouser' => $tipouser,
							'usuario' => $usuResult,
							//---> Variables de Interrogatorio
							'AlergiaMedica' => $AlergiaMedica,
							'DescripAlerMed' => $DescripAlerMed,
							'Anestesia' => $Anestesia,
							'ProblemAnestesia' => $ProblemAnestesia,
							'DescripAnes' => $DescripAnes,
							'TomaMedicamento' => $TomaMedicamento,
							'DescripMed' => $DescripMed,
							'Adicciones' => $Adicciones,
							'CualesAdicc' => $CualesAdicc,
							'Fumador' => $Fumador,
							'BebAlcohol' => $BebAlcohol,
							'Embarazada' => $Embarazada,
							'MesesGesta' => $MesesGesta,
							'ProblCPeriodo' => $ProblCPeriodo,
							'OtraEnferm' => $OtraEnferm);

						switch ($_SESSION["user"]->Tipo_Usuario) {
							case '1':
								//echo "Si a todo";
								break;
							
							case '2':
								//echo "Solo clientes";
								break;
							
							case '3':
								//echo "tu nomas";
								break;
							
							default:
								//echo "Default";
								break;
						}

						//---> Llamando a la vista en FrontEnd con envío de datos por parámetro
						$this->load->view('AdminHeader',$dataSession);
						$this->load->view('contenidoPerfil',$datosAModificar);
						$this->load->view('AdminFooter');
					}
				}
			}
		}

		public function actualiza_perfil(){

			$respuesta["err"]="0";
			//$respuesta["msg"]="Hola";
			//$respuesta["post"]=($_POST);
			//$respuesta["get"]=($_GET);
			$respuesta["request"]=($_REQUEST);

			if ( $_POST['Pass_Usuario'] == "" ){
				unset($_POST['Pass_Usuario']);
			}

			if ( $_POST['Imagen_Usuario'] == "" ){
				unset($_POST['Imagen_Usuario']);
			}

			if ( $_GET['idPerfil'] == ""){
				$respuesta["err"] = !$this->Primero->agregar_usuario( $_POST );
				$respuesta["flow"] = "agregar_usuario";
			} else {
				$respuesta["err"] = !$this->Primero->actualiza_perfil( $_GET['idPerfil'], $_POST );
				$respuesta["flow"] = "actualiza_perfil";
			}

			//var_dump($respuesta);
			echo json_encode($respuesta);

		}

		public function cambiar_activo(){

			$respuesta["err"]="0";
			//$respuesta["msg"]="Hola";
			$respuesta["post"]=($_POST);
			//$respuesta["get"]=($_GET);
			$respuesta["request"]=($_REQUEST);
			//var_dump($_POST['chk']);
			if ($_POST['chk'] == "false")
				$_POST['chk'] = false;
			if ($_POST['chk'] == "true")
				$_POST['chk'] = true;

			$arrayStatus = array('Activo_Usuario' => $_POST['chk'] );
			$respuesta["err"] = !$this->Primero->cambiar_status( $_POST['idUsu'], $arrayStatus );

			//var_dump($respuesta);
			echo json_encode($respuesta);

		}

		public function add_Expediente(){

			$respuesta["error"]=false;
			//$respuesta["msg"]="Hola";
			//$respuesta["post"]=($_POST);
			$respuesta["get"]=($_GET);
			// $respuesta["request"]=($_REQUEST);
			//var_dump($_POST['chk']);

			// $arrayStatus = array('Activo_Usuario' => $_POST['chk'] );
			$respuesta["error"] = !$this->Primero->addExpediente( $_GET['idPerfil'] );

			//var_dump($respuesta);
			echo json_encode($respuesta);

		}

	}
?>