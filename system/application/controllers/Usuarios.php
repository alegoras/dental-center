<?php 

	/**
	* 
	*/
	class Usuarios extends CI_Controller
	{
		
		function __construct()
		{
			parent::__construct();
			//var_dump( $_SESSION);
			$this->load->library('session');
			//echo $this->session->username;;
			IF (! $this->session->has_userdata("user")) {
				echo '<meta http-equiv="refresh" content="0;url=https://pruebasal.000webhostapp.com/">';
			} else {
				if ($_SESSION["user"]->Tipo_Usuario <> 1) {
					echo '<meta http-equiv="refresh" content="0;url='. base_url() .'Admin/Inicio">';
				} else{
					$this->load->model("Primero");
				}
			}
		}

		public function MostrarUsuarios(){

			$datosUsu = $this->Primero->get_perfil($_SESSION["user"]->Email_Usuario);

			//---> Obteniendo lista de todos los usuarios
			//$result = $this->db->get("USUARIO");

			$data = array(
				'tituloVista' => 'Usuarios',
				'titulo1' => 'Dental',
				'titulo2' => 'Admin',
				'usuario' => $datosUsu->result());
			
			$this->load->view('AdminHeader',$data);

			$result = $this->Primero->todos_usuarios();

			$dataUsuarios = array(
				'consulta' => $result);

			//---> Llamando a la vista en FrontEnd
			//$this->load->view("Usuarios",$data);
			$this->load->view('Usuarios',$dataUsuarios);
			$this->load->view('AdminFooter');
		}

	}
 ?>