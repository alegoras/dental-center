<?php

	class Admin extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$this->load->library('session');
			$this->load->model("Primero");
			//var_dump($_SESSION["user"]);
			IF (! $this->session->has_userdata("user")) {
				echo '<meta http-equiv="refresh" content="0;url=https://pruebasal.000webhostapp.com/">';
			}
		}

		public function Inicio()
		{
			$datosUsu = $this->Primero->get_perfil($_SESSION["user"]->Email_Usuario);
			
			$data = array(
				'tituloVista' => 'Inicio',
				'titulo1' => 'Dental',
				'titulo2' => 'Admin',
				'usuario' => $datosUsu->result());
			
			$this->load->view('AdminHeader',$data);

			$data2 = array(
				'usuario' => $datosUsu->result());

			$this->load->view('PrincipalContent',$data2);
			$this->load->view('AdminFooter');
			
		}
	}
?>