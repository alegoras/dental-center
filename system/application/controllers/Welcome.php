<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
		parent::__construct();
		$this->load->helper("url");
		$this->load->helper("form");
		$this->load->model("primero");
		$this->load->library("session");
	}

	public function index()
	{
		$this->load->database();
		$this->load->view('welcome_message');
		$query = $this->db->query('SELECT * FROM TIPO_USUARIO');

		foreach ($query->result() as $row)
		{
		        echo $row->Descripcion_TipoUsu."<br>";
		        //echo $row->name;
		        //echo $row->email;
		}

	}

	public function login()
	{
		//$this->load->view('welcome_message');
		echo "login";
		$this->load->model("primero");
		$users = $this->primero->todos_usuarios();
		#var_dump($users);/*
		foreach ($users as $nombre => $tipo) {
			# code...
			#var_dump($tipo);
			echo "<br>Nombre: ".$tipo->Nombre_Usuario ."<br>";
		}
	}

	public function check_login($u_email, $pass)
	{
		//$this->load->view('welcome_message');
		#echo $u_email,$pass;# = $_REQUEST['email'];
		$this->load->model("primero");
		$filtros = array('Email_Usuario' => $u_email, 'Pass_Usuario' => $pass );
		$check = $this->primero->check_login($filtros);
		#var_dump($check);
		echo json_encode($check);
	}

	public function start_sess()
	{
		$user_email = $this->input->post('exampleInputEmail2');
		$user_pass = $this->input->post('exampleInputPassword2');
		$result = $this->get_user($user_email,$user_pass);

		if ($result->num_rows() > 0){
			
			$_SESSION["user"]=$result->result()[0];
			$newdata = array(
				'username' => $_SESSION["user"]->Nombre_Usuario,
				'tipouser' => $_SESSION["user"]->Tipo_Usuario,
				'email' => $_SESSION["user"]->Email_Usuario);
			$this->session->set_userdata($newdata);
			//var_dump($_SESSION["user"]);
			$data = array(
				'tituloVista' => 'Inicio',
				'titulo1' => 'Dental',
				'titulo2' => 'Admin',
				'perfil' => $_SESSION["user"]->Nombre_Usuario,
				'tipouser' => $_SESSION["user"]->Tipo_Usuario);

			//$this->load->view("admin",$data);
			echo '<meta http-equiv="refresh" content="0;url=https://pruebasal.000webhostapp.com/system/Admin/Inicio">';
		} else{
			echo '<meta http-equiv="refresh" content="0;url=https://pruebasal.000webhostapp.com/">';
		}
	}

	public function get_user($user_email,$user_pass)
	{
		#$user_email = $this->input->post('exampleInputEmail2');
		#$user_pass = $this->input->post('exampleInputPassword2');
		$filtros = array('Email_Usuario' => $user_email, 'Pass_Usuario' => $user_pass );
		$result = $this->primero->get_user($filtros);
		return $result;
	}

	public function get_user_api()
	{
		$user_email = $this->input->post('exampleInputEmail2');
		$user_pass = $this->input->post('exampleInputPassword2');

		$filtros = array('Email_Usuario' => $user_email, 'Pass_Usuario' => $user_pass );

		$result = $this->primero->get_user($filtros);
		
		echo (json_encode($result));
	}

	public function mostrar_sesion()
	{
		var_dump($_SESSION);
		echo $this->session->username;
	}

}
