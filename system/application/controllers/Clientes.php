<?php
	
	/**
	* Esta clase permite ver todos los usuarios con tipo 3
	*/
	class Clientes extends CI_Controller
	{
		
		function __construct()
		{
			parent::__construct();
			$this->load->library('session');
			IF (! $this->session->has_userdata("user")) {
				echo '<meta http-equiv="refresh" content="0;url=https://pruebasal.000webhostapp.com/">';
			} else {
				if ($_SESSION["user"]->Tipo_Usuario == 3) {
					echo '<meta http-equiv="refresh" content="0;url='. base_url() .'Admin/Inicio">';
				} else{
					$this->load->model("Primero");
				}
			}
		}

		public function todos(){
			//---> Obteniendo Usuario actual
			$datosUsu = $this->Primero->get_perfil($_SESSION["user"]->Email_Usuario);
			
			//---> Obteniendo todos los clientes
			$clientes = $this->Primero->get_clientes();

			//---> Colocando los datosa enviar a la cabecera
			$data = array(
				'tituloVista' => 'Clientes',
				'titulo1' => 'Dental',
				'titulo2' => 'Admin',
				'usuario' => $datosUsu->result());

			$clientesBody = array(
				'consulta' => $clientes);

			$this->load->view('AdminHeader',$data);
			$this->load->view('ClientesContent',$clientesBody);
			$this->load->view('AdminFooter');
		}
	}
?>