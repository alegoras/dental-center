<?php

	class Citas extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			$this->load->library('session');
			IF (! $this->session->has_userdata("user")) {
				echo '<meta http-equiv="refresh" content="0;url=https://pruebasal.000webhostapp.com/">';
			} else {
				$this->load->model("Primero");
			}
		}

		public function VerCitas()
		{
			
			$datosUsu = $this->Primero->get_perfil($_SESSION["user"]->Email_Usuario);

			$data = array(
				'tituloVista' => 'Inicio',
				'titulo1' => 'Dental',
				'titulo2' => 'Admin',
				'usuario' => $datosUsu->result());
			
			$this->load->view('AdminHeader',$data);

			$data2 = $datosUsu->result();

			if ($data2[0]->Tipo_Usuario != 3) {
				//Identificando al Doctor
				$datosUsu = $this->Primero->get_perfil($_SESSION["user"]->Email_Usuario);
				$data2 = $datosUsu->result();

				//Si es administrador puede ver todas las citas en la TABLA DINAMICA, de lo contrario, solo las citas asociadas.
				if ($data2[0]->Tipo_Usuario == 1) {
					//Extrayendo todas las citas
					$datosCitas = $this->Primero->VerCitasTodas();

					//Almacenando resultado y consultando nombre del Doctor a cargo
					$result = $datosCitas->result();

					foreach ($result as $fila) {
						//Obteniendo nombre de doctor
						$res = $this->nombreDoc($fila->ID_UsuDent);
						$nom = $res[0]->Nombre_Usuario . " " . $res[0]->Nombre2_Usuario;

						//Obteniendo nombre de cliente
						$res = $this->nombreCli($fila->ID_Usuario);
						$nomCli = $res[0]->Nombre_Usuario . " " . $res[0]->Nombre2_Usuario;

						//Reemplazando el valor ya en la variable fila
						$fila->ID_UsuDent = $nom;
						$fila->ID_Usuario = $nomCli;
					}

					//Colocando el resultado de la extracción en variable y llamada a contenido
					$citas = array('citasConsulta' => $datosCitas->result());
					$this->load->view('CitasContent',$citas);
				} else{
					//Extrayendo todas las citas asociadas con este doctor
					$datosCitas = $this->Primero->VerCitasDoc($data2[0]->ID_Usuario);

					//Almacenando resultado y consultando nombre del Doctor a cargo
					$result = $datosCitas->result();

					foreach ($result as $fila) {
						//Obteniendo nombre de doctor
						$res = $this->nombreDoc($fila->ID_UsuDent);
						$nom = $res[0]->Nombre_Usuario . " " . $res[0]->Nombre2_Usuario;

						//Obteniendo nombre de cliente
						$res = $this->nombreCli($fila->ID_Usuario);
						$nomCli = $res[0]->Nombre_Usuario . " " . $res[0]->Nombre2_Usuario;

						//Reemplazando el valor ya en la variable fila
						$fila->ID_UsuDent = $nom;
						$fila->ID_Usuario = $nomCli;
					}

					//Colocando el resultado de la extracción en variable y llamada a contenido
					$citas = array('citasConsulta' => $datosCitas->result());
					
					$this->load->view('CitasContent',$citas);
				}
			} else{
				//Extrayendo todas las citas asociadas con este cliente
				$datosCitas = $this->Primero->VerCitasCli($data2[0]->ID_Usuario);

				//Almacenando resultado y consultando nombre del Doctor a cargo
				$result = $datosCitas->result();

				foreach ($result as $fila) {
					//Obteniendo nombre de doctor
					$res = $this->nombreDoc($fila->ID_UsuDent);
					$nom = $res[0]->Nombre_Usuario . " " . $res[0]->Nombre2_Usuario;

					//Reemplazando el valor ya en la variable fila
					$fila->ID_UsuDent = $nom;
				}
				
				//Colocando el resultado ya modificado en variable y se llama a contenido
				$citas = array('citasConsulta' => $result);
				$this->load->view('CitasCliente',$citas);
			}
			$this->load->view('AdminFooter');
			
		}

		public function confirmarCita(){

			$respuesta["err"]="0";
			//$respuesta["msg"]="Hola";
			$respuesta["post"]=($_POST);
			//$respuesta["get"]=($_GET);
			$respuesta["request"]=($_REQUEST);
			//var_dump($_POST['chk']);
			if ($_POST['chk'] == "false")
				$_POST['chk'] = 'Pendiente';
			if ($_POST['chk'] == "true")
				$_POST['chk'] = 'Confirmado';

			$arrayStatus = array('ID_EstatusCita' => $_POST['chk'] );
			$respuesta["err"] = !$this->Primero->confirmarCita( $_POST['idCit'], $arrayStatus);

			//var_dump($respuesta);
			echo json_encode($respuesta);

		}

		public function nombreDoc($idDoc){
			$nomDoc = $this->Primero->get_nomDoc($idDoc);
			return $nomDoc->result();
		}

		public function nombreCli($idCli){
			$nomCli = $this->Primero->get_nomCli($idCli);
			return $nomCli->result();
		}
		
		public function obtenerCitas(){
			//Esta función muestra en el calendario, sólo las citas asociadas al doctor
			$datosUsu = $this->Primero->get_perfil($_SESSION["user"]->Email_Usuario);
			$data2 = $datosUsu->result();

			//Extrayendo todas las citas asociadas con este doctor
			$datosCitas = $this->Primero->VerCitasDoc($data2[0]->ID_Usuario);

			foreach ($datosCitas->result() as $fila) {
				//Obteniendo nombre de doctor
				$res = $this->Primero->get_nomCli($fila->ID_Usuario)->result();
				$nom = $res[0]->Nombre_Usuario . " " . $res[0]->Nombre2_Usuario;

				//Reemplazando el valor ya en la variable fila
				$fila->ID_Usuario = $nom;

				if ($fila->ID_EstatusCita == 'Confirmado'){
					$color = "#01DF01";
				}else if ($fila->ID_EstatusCita == 'Pendiente') {
					$color = "#FFBF00";
				} else{
					$color = "#FF4000";
				}

				$datosJson[] = array(
					'id' => $fila->ID_Cita,
					'title' => $fila->ID_Usuario,
					'start' => $fila->HraIni_Citas,
					'end' => $fila->HraFin_Citas,
					'color' => $color
					);
			}
			echo json_encode($datosJson);
		}

		public function add_event() 
		{
		    /* Our calendar data */
		    $idCli = $_POST['ID_Usuario'];
		    $start_date = $_POST['HraIni_Citas'];
		    $end_date = $_POST['HraFin_Citas'];
		    $recur = $_POST['Recurrente_Cita'];
		    $cont = $_POST['Cant_Citas'];

		    if($recur==1){
		    	$est="";
		    } else if ($recur==2) {
		    	$est="Semana";
		    	$dias = 7;
		    } else if ($recur==3) {
		    	$est="Quincena";
		    	$dias = 14;
		    } else{
		    	$est="Mes";
		    	$dias = 28;
		    }

		    $newStart = new DateTime($start_date);
		    $newEnd = new DateTime($end_date);

	    	//$newStart->add(new DateInterval('P'.$dias.'D'));

		    echo $newStart->format('Ymd H:i');
		    echo $newEnd->format('Ymd H:i');

		    if ($recur!=1 && ($cont!="" || $cont != 0)) {

		    	//Ciclando para citas recurrentes
		    	$idPadre=0;
		    	for ($i=1; $i<=$cont ; $i++) { 
		    		$this->Primero->add_event(array(
				    	"ID_Cita" => "",
				       	"ID_Usuario" => $idCli,
				       	"ID_UsuDent" => $_SESSION["user"]->ID_Usuario,
				       	"HraIni_Citas" => $newStart->format('Y-m-d H:i'),
				       	"HraFin_Citas" => $newEnd->format('Y-m-d H:i'),
				       	"ID_EstatusCita" => 'Pendiente',
				       	"Recurrente_Cita" => $est,
				       	"ID_Recurrencia" => $idPadre
				       	)
				    );

		    		if ($idPadre==0){
		    			//Obteniendo el ultimo registro con el ultimo ID_Cita
				    	$result = $this->Primero->VerCitasTodas()->result();

				    	foreach ($result as $fila) {
				    		$idPadre = $fila->ID_Cita;
				    	}
		    		}

		    		if ($dias == 28){
		    			$meses = 1;
		    			$newStart->add(new DateInterval('P'.$meses.'M'));
				    	$newEnd->add(new DateInterval('P'.$meses.'M'));
		    		} else{
					    $newStart->add(new DateInterval('P'.$dias.'D'));
					    $newEnd->add(new DateInterval('P'.$dias.'D'));
					}
		    	}
		    } else {
		    	$this->Primero->add_event(array(
			    	"ID_Cita" => "",
			       	"ID_Usuario" => $idCli,
			       	"ID_UsuDent" => $_SESSION["user"]->ID_Usuario,
			       	"HraIni_Citas" => $start_date,
			       	"HraFin_Citas" => $end_date,
			       	"ID_EstatusCita" => 'Pendiente',
			       	"Recurrente_Cita" => $est
			       	)
			    );
		    }
		    //$insert_id = $this->db->insert_id();
		    //var_dump($insert_id);
		    echo 'Eeeeeexito!';
		}

		public function modModal_event() 
		{
		    /* Our calendar data */
		    $idCita = $_POST['ID_CitaM'];
		    $start_date = $_POST['HraIniM_Citas'];
		    $end_date = $_POST['HraFinM_Citas'];

		    echo $_POST['ID_EstatusCitas'];

		    if ($_POST['ID_EstatusCitas'] != "") {
		    	$estatus = $_POST['ID_EstatusCitas'];
		    } else{
		    	$estatus = "Pendiente";
		    }

		    echo $estatus;

		    $newStart = new DateTime($start_date);
		    $newEnd = new DateTime($end_date);

		    echo $newStart->format('Ymd H:i');
		    echo $newEnd->format('Ymd H:i');

	    	if ($this->Primero->update_event($idCita,array(
		       	"HraIni_Citas" => $start_date,
		       	"HraFin_Citas" => $end_date,
		       	"ID_EstatusCita" => $estatus
		       	)
		    ) == true) {
	    		echo 'Cita modificada';
		    }
		}

		public function mod_event()
		{
			/* Our calendar data */
		    //var_dump($_POST);
		    $idCli = $_POST['ID_Cita'];
		    if ($idCli != "") {
		    	$datos = array(
		    	'HraIni_Citas' => $_POST['HraIni_Citas'],
		    	'HraFin_Citas' => $_POST['HraFin_Citas']
		    	);

			    IF($this->Primero->update_event($idCli,$datos) == true){
			    	echo "modificado";
			    }
		    }
		}

		public function cancela_Cita(){
			$id = $_POST['idCit'];
			
			$datos = array(
		    	'ID_EstatusCita' => 'Cancelado'
		    	);

			IF($this->Primero->update_event($id,$datos) == true){
			    	echo "modificado";
			}
		}
	}
?>