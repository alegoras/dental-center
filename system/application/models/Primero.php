<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Primero extends CI_Model {

        public function __construct()
            {
                parent::__construct();
                $this->load->database();
                $this->db->reconnect();
            }
            
        public $title;
        public $content;
        public $date;

        public function todos_usuarios()
        {
            $query = $this->db->select('*')->from('USUARIO')
                ->where('Tipo_Usuario', 1)
                ->or_where('Tipo_Usuario', 2)
                ->get();
            return $query;
        }

        public function check_login($filtros)
        {
                #var_dump($filtros);
                $query = $this->db->get_where('USUARIO', $filtros /*, $limit, $offset*/);
                #var_dump($query);
                if ($query->num_rows() > 0)
                    return true;
                else
                    return false;
        }

        public function get_user($filtros)
        {
                $query = $this->db->get_where('USUARIO', $filtros /*, $limit, $offset*/);
                return $query;
        }

        public function update_entry()
        {
                $this->title    = $_POST['title'];
                $this->content  = $_POST['content'];
                $this->date     = time();

                $this->db->update('entries', $this, array('id' => $_POST['id']));
        }

        public function get_tipousu($idUsu)
        {
            $query = $this->db->select('*')->from('TIPO_USUARIO')
                ->where('ID_TipoUsu', $idUsu)
                ->get();

            return $query;
        }

        public function get_perfil($em_usu)
        {
            $query = $this->db->select('*')->from('USUARIO')
                ->where('Email_Usuario', $em_usu)
                ->get();
            return $query;
        }

        public function get_clientes()
        {
            $query = $this->db->select('*')->from('USUARIO')
                ->where('Tipo_Usuario', 3)
                ->get();
            return $query;
        }

        public function actualiza_perfil($idPerfil , $datosPerfil)
        {
            $this->db->where('ID_Usuario', $idPerfil);
            return $this->db->update('USUARIO', $datosPerfil);
        }

        public function agregar_usuario( $datosPerfil )
        {
            //$this->db->where('ID_Usuario', $idPerfil);
            return $this->db->insert('USUARIO', $datosPerfil);
        }

        public function cambiar_status( $idPerfil,$status )
        {
            //$this->db->where('ID_Usuario', $idPerfil);
            //return $this->db->insert('USUARIO', $datosPerfil);
            //$this->db->set('Activo_Usuario', $status); 
            $this->db->where('ID_Usuario', $idPerfil);
            return $this->db->update('USUARIO', $status);
        }

        /*----------- Funciones para extraccion de datos para el Calendario -----------*/
        public function VerCitasTodas()
        {
            $query = $this->db->select('*')->from('CITAS')
                ->where('ID_EstatusCita !=', 'Cancelado')
                ->get();
            return $query;
        }

        public function VerCitasDoc($idPerfil)
        {
            $query = $this->db->select('*')->from('CITAS')
                ->where('ID_UsuDent', $idPerfil)
                ->get();
            return $query;
        }

        public function VerCitasCli($idPerfil)
        {
            $query = $this->db->select('*')->from('CITAS')
                ->where('ID_Usuario', $idPerfil)
                ->where('ID_EstatusCita !=', 'Cancelado')
                ->get();
            return $query;
        }

        public function confirmarCita( $idCita,$status )
        {
            //$this->db->where('ID_Usuario', $idPerfil);
            //return $this->db->insert('USUARIO', $datosPerfil);
            //$this->db->set('Activo_Usuario', $status); 
            $this->db->where('ID_Cita', $idCita);
            return $this->db->update('CITAS', $status);
        }

        public function get_nomDoc($idDoc)
        {
            $query = $this->db->select('Nombre_Usuario,Nombre2_Usuario')->from('USUARIO')
                ->where('ID_Usuario', $idDoc)
                ->get();
            return $query;
        }

        public function get_nomCli($idCli)
        {
            $query = $this->db->select('Nombre_Usuario,Nombre2_Usuario')->from('USUARIO')
                ->where('ID_Usuario', $idCli)
                ->get();
            return $query;
        }

        public function get_events($start, $end)
        {
            return $this->db->where("HraIni_Citas >=", $start)->where("HraFin_Citas <=", $end)->get("CITAS");
        }

        public function add_event($data)
        {
            $this->db->insert("CITAS", $data);
        }

        public function get_event($id)
        {
            return $this->db->where("ID_Cita", $id)->get("CITAS");
        }

        public function update_event($id, $data)
        {
            $this->db->where("ID_Cita", $id);
            return $this->db->update("CITAS", $data);
        }

        public function delete_event($id)
        {
            $this->db->where("ID_Cita", $id)->delete("CITAS");
        }
        /*---------- Final de funciones para Calendario -------------*/

        /*---------------- Funciones para expedientes --------------*/
        public function get_Expediente($idUsu)
        {
            $query = $this->db->select('Expediente_Usuario')->from('USUARIO')
                ->where('ID_Usuario', $idUsu)
                ->get();
            return $query;
        }

        public function get_Interrogatorio($idExp)
        {
            $query = $this->db->select('*')->from('INTERROGATORIO')
                ->where('ID_Interr', $idExp)
                ->get();
            return $query;
        }

        public function verExpediente($idCli){
            $query = $this->db->select('*')->from('EXPEDIENTE')
                ->where('UsuarioId_Expediente', $idCli)
                ->get();
            return $query;
        }

        public function addExpediente($idUser){
            $this->db->where('ID_Expediente', 1); 
            $res = $this->db->get('EXPEDIENTE');
            $newID =  $res->result()[0]->Num_Expediente + 1;

            $this->db->set('Expediente_Usuario', $newID );
            $this->db->where('ID_Usuario', $idUser);
            $this->db->update('USUARIO');

            $this->db->set('Num_Expediente', $newID );
            $this->db->where('ID_Expediente', 1);
            return $this->db->update('EXPEDIENTE');
        }
        /*------------ Final Funciones para expedientes ------------*/
}
?>