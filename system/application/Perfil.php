<?php
	/**
	* Controlador para perfil de usuario
	*/
	class Perfil extends CI_Controller
	{
		
		function __construct()
		{
			parent::__construct();
			$this->load->library('session');
			IF (! $this->session->has_userdata("user")) {
				echo '<meta http-equiv="refresh" content="0;url=https://pruebasal.000webhostapp.com/">';
			} else {
				$this->load->model("Primero");
			}
		}

		public function Ver(){
			//---> Obteniendo todos los datos del usuario

			//---> Colocando los datos genericos y del usuario en un arreglo
			$data = array(
				'tituloVista' => 'Perfil',
				'titulo1' => 'Dental',
				'titulo2' => 'Admin',
				'perfil' => $_SESSION["user"]->Nombre_Usuario,
				'tipouser' => $_SESSION["user"]->Tipo_Usuario,
				'consulta' => $_SESSION["user"]);

			//---> Llamando a la vista en FrontEnd con envío de datos por parámetro
			$this->load->view("Perfil",$data);

		}
	}
?>