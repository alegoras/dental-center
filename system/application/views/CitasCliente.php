      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
		  <div class="row">
				<div class="col-lg-12">
					<h3 class="page-header"><i class="icon_calendar"></i> Mis citas</h3>
					<ol class="breadcrumb">
						<li><i class="fa fa-home"></i><a href="<?= base_url() . 'Admin/Inicio' ?>">Inicio</a></li>
						<li><i class="fa fa-calendar"></i>Mis citas</li>
					</ol>
				</div>
			</div>
              <!-- page start-->
              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              Citas
                          </header>
                          <div class="table-responsive">
                            <table id="tab-cit" class="table table-striped table-advance table-hover">
                             <thead>
                              <tr>
                                 <th><i class="icon_calendar"></i> Fecha</th>
                                 <th><i class="icon_clock_alt"></i> De las</th>
                                 <th><i class="icon_clock_alt"></i> A las</th>
                                 <th><i class="icon_profile"></i> Doctor</th>
                                 <th><i class="icon_info_alt"></i> Confirmada</th>
                                 <th><i class="icon_toolbox_alt"></i> Acciones</th>
                              </tr>
                             </thead>
                             <tbody>
                                <!-- Lista de usuarios -->
                                <?php 
                                  foreach ($citasConsulta as $fila) {
                                ?>
                                  <tr>
                                    <td>
                                    <?php
                                      setlocale(LC_ALL,"es_MX");
                                      echo utf8_encode(strftime("%A %d de %B del %Y",strtotime($fila->HraIni_Citas)));
                                    ?>
                                    </td>
                                    <td><?= substr($fila->HraIni_Citas,11,5) ?></td>
                                    <td><?= substr($fila->HraFin_Citas,11,5) ?></td>
                                    <td><?= $fila->ID_UsuDent ?></td>
                                    <td>
                                      <center><input type="checkbox" class="activar" name="act-cit<?= $fila->ID_Cita ?>" id="act-cit" value="<?= $fila->ID_Cita ?>"
                                          <?php
                                            if ($fila->ID_EstatusCita == "Confirmado") {
                                              echo 'checked';
                                            }
                                          ?>
                                        /></center>
                                    </td>
                                    <td>
                                      <div class="btn-group">
                                          <a class="btn btn-danger" id="cancelaCita" name="cancelaCita" data-toggle="Tooltip" title="Cancelar Cita" onclick="Cancelar(<?= $fila->ID_Cita ?>)"><i class="icon_close_alt2"></i></a>
                                      </div>
                                    </td>
                                  </tr>
                                <?php
                                  }
                                ?>
                             </tbody>
                          </table>
                        </div>
                      </section>
                  </div>
              </div>
              <!-- page end-->
          </section>
      <!--main content end-->

      <!-- javascripts -->
    <!--custom checkbox & radio-->
    <script type="text/javascript" src="<?= base_url() ?>CosasNiceAdmin/js/ga.js"></script>
    <!--custom switch-->
    <script src="<?= base_url() ?>CosasNiceAdmin/js/bootstrap-switch.js"></script>
    <!--custom tagsinput-->
    <script src="<?= base_url() ?>CosasNiceAdmin/js/jquery.tagsinput.js"></script>
    <!-- bootstrap-wysiwyg -->
    <script src="<?= base_url() ?>CosasNiceAdmin/js/jquery.hotkeys.js"></script>
    <script src="<?= base_url() ?>CosasNiceAdmin/js/bootstrap-wysiwyg.js"></script>
    <script src="<?= base_url() ?>CosasNiceAdmin/js/bootstrap-wysiwyg-custom.js"></script>
    <!-- ck editor -->
    <script type="text/javascript" src="<?= base_url() ?>CosasNiceAdmin/assets/ckeditor/ckeditor.js"></script>
    
    <!-- Este script es escencial para que funcione el SWITCH -->
    <!-- custom form component script for this page-->
    <script src="<?= base_url() ?>CosasNiceAdmin/js/form-component.js"></script>
    <script src="<?= base_url() ?>CosasNiceAdmin/js/tablaDinamica.js"></script>
    <script>
    
    $(document).ready(function () {
        $('.activar').click(function(event){
          var idSwitchUsu = $(this).val()
          var esteElemento = $(this)
          var esteChBx = $(this).prop('checked')
          var jqxhr = $.post("<?= base_url() ?>Citas/confirmarCita", {idCit:idSwitchUsu, chk:esteChBx}, function(data) {
          },"json")
            .done(function(data) {
              console.log(data);

              bootbox.alert({
                message: "Confirmación cambiada",
                callback: function(){ location.reload() }
              });
            })
            .fail(function(a,b,c) {
              console.log(a,b,c);
              bootbox.alert( "Ocurrió un error al cambiar el estatus. Inténtelo de nuevo." );
              esteElemento.prop( "checked", !esteChBx);
            })
        });

        $('#tab-cit').DataTable( {
            //Opcion para ordenar [[columna(int)],[orden(string)]]
            "order": [[ 0, "desc" ]],

            //--> Controlando el lenguaje
            language: {
                processing:     "Procesando...",
                search:         "Buscar:",
                lengthMenu:     "Mostrar _MENU_ registros",
                info:           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                infoEmpty:      "Mostrando registros del 0 al 0 de un total de 0 registros",
                infoFiltered:   "(filtrado de un total de _MAX_ registros)",
                infoPostFix:    "",
                loadingRecords: "Cargando...",
                zeroRecords:    "No se encontraron resultados",
                emptyTable:     "Ningún dato disponible en esta tabla",
                paginate: {
                    first:      "Primero",
                    previous:   "Anterior",
                    next:       "Siguiente",
                    last:       "Último"
                },
                aria: {
                    sortAscending:  ": Activar para ordenar la columna de manera ascendente",
                    sortDescending: ": Activar para ordenar la columna de manera descendente"
                }
            }
            //<---
        });
    });
    
    function Cancelar(id){

      if (confirm("¿Esta seguro de cancelar esta cita?") == true){
        var jqxhr = $.post("<?= base_url() ?>Citas/cancela_Cita", {idCit:id}, function(data) {
        },"text")
        .done(function(data) {
          console.log(data);

          bootbox.alert({
            message: "Cita cancelada!",
            callback: function(){ location.reload() }
          });
        })
        .fail(function(a,b,c) {
          console.log(a,b,c);
          bootbox.alert( "Ocurrió un error al cancelar la cita. Inténtelo de nuevo." );
        })
      } 
    }

    </script>