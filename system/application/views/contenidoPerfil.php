<!--main content start-->
<section id="main-content">
  <section class="wrapper">
	  <div class="row">
			<div class="col-lg-12">
				<h3 class="page-header"><i class="fa fa-user-md"></i> Mi Perfil</h3>
				<ol class="breadcrumb">
					<li><i class="fa fa-home"></i><a href="<?= base_url() ?>Admin/Inicio">Inicio</a></li>
					<!-- <li><i class="icon_documents_alt"></i>Pages</li> -->
					<li><i class="fa fa-user-md"></i>Perfil</li>
				</ol>
			</div>
		</div>
    <div class="row">
      <!-- profile-widget -->
      <div class="col-lg-12">
        <div class="profile-widget profile-widget-info">
          <div class="panel-body">
            <div class="col-lg-2 col-sm-2">
              <h4><?= $usuario[0]->Nombre_Usuario ?></h4>               
              <div class="follow-ava">
                <img src="data:image/jpeg;base64, <?= $usuario[0]->Imagen_Usuario ?>"/>
              </div>
              <h6><?= $tipouser[0]->Descripcion_TipoUsu ?></h6>
            </div>
            <div class="col-lg-4 col-sm-4 follow-info">
              <p><?= $usuario[0]->Nombre_Usuario ?> <?= $usuario[0]->Nombre2_Usuario ?></p>
              <p><span><i class="icon_mail_alt"></i> <?= $usuario[0]->Email_Usuario ?></span></p>
              <p><span><i class="icon_phone"></i> <?= $usuario[0]->Telefono1_Usuario ?> / <?= $usuario[0]->Telefono2_Usuario ?> </span></p>
              <?php
              if ($usuario[0]->Expediente_Usuario != 0) {
              ?>
              <p><span><i class="icon_document_alt"></i> Expediente #<?=$usuario[0]->Expediente_Usuario?></span></p>
              <?php
              } else{
              ?>
              <p><span><i class="icon_pin_alt"></i> <?= $usuario[0]->Ciudad_Usuario ?></span></p>
              <?php
              }
              ?>
            </div>
						<div class="col-lg-2 col-sm-6 follow-info weather-category">
              <ul>
                <li class="active">
                  <i class="fa fa-tachometer fa-2x"> </i><br>
  									  Información general
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- page start-->
    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading tab-bg-info">
            <ul class="nav nav-tabs">
              <li class="active">
                <a data-toggle="tab" href="#profile">
                  <p><span><i class="icon_info_alt"></i>
                  Información
                  </span></p>
                </a>
              </li>
              <li class="">
                <a data-toggle="tab" href="#edit-profile">
                  <p><span><i class="icon_pencil"></i>
                  Editar Perfil
                  </span></p>
                </a>
              </li>
              <?php
              if ($_SESSION["user"]->Tipo_Usuario != 3) {
              ?>
              <li class="">
                <a data-toggle="tab" href="#expediente-profile">
                  <p><span><i class="icon_document_alt"></i>
                  Expediente
                  </span></p>
                </a>
              </li>
              <?php
              }
              ?>
            </ul>
          </header>
          <div class="panel-body">
            <div class="tab-content">
              <!-- profile -->
              <div id="profile" class="tab-pane active">
                <section class="panel">
                  <div class="bio-graph-heading">
                    Hola <?= $usuario[0]->Nombre_Usuario ?>
                  </div>
                  <div class="panel-body bio-graph-info">
                    <h1>Datos Generales</h1>
                    <div class="row">
                      <div class="bio-row">
                        <p><span>Nombre: </span><?= $usuario[0]->Nombre_Usuario ?> </p>
                      </div>
                      <div class="bio-row">
                        <p><span>Apellidos: </span><?= $usuario[0]->Nombre2_Usuario ?> </p>
                      </div>
                      <div class="bio-row">
                        <p><span>Género: </span><?= $usuario[0]->Genero_Usuario ?> </p>
                      </div>
                      <div class="bio-row">
                        <p><span>Edad: </span><?= $usuario[0]->Edad_Usuario ?> </p>
                      </div>
                      <div class="bio-row">
                        <p><span>Estado civil: </span><?= $usuario[0]->EstadoCivil_Usuario ?> </p>
                      </div>
                      <div class="bio-row">
                        <p><span>Dirección: </span><?= $usuario[0]->Direccion_Usuario ?> </p>
                      </div>
                      <div class="bio-row">
                        <p><span>Colonia: </span><?= $usuario[0]->Colonia_Usuario ?> </p>
                      </div>
                      <div class="bio-row">
                        <p><span>Ciudad: </span><?= $usuario[0]->Ciudad_Usuario ?> </p>
                      </div>
                      <div class="bio-row">
                        <p><span>Estado: </span><?= $usuario[0]->Region_Usuario ?> </p>
                      </div>
                      <div class="bio-row">
                        <p><span>Código postal: </span><?= $usuario[0]->CodigoPostal_Usuario ?> </p>
                      </div>
                      <div class="bio-row">
                        <p><span>Ocupación: </span><?= $usuario[0]->Ocupacion_Usuario ?> </p>
                      </div>
                      <div class="bio-row">
                        <p><span>Email: </span><?= $usuario[0]->Email_Usuario ?> </p>
                      </div>
                      <div class="bio-row">
                        <p><span>Teléfono 1: </span><?= $usuario[0]->Telefono1_Usuario ?> </p>
                      </div>
                      <div class="bio-row">
                        <p><span>Teléfono 2: </span><?= $usuario[0]->Telefono2_Usuario ?> </p>
                      </div>
                      <div class="bio-row">
                        <p><span>Fecha de nacimiento: </span><?= $usuario[0]->FechaNacim_Usuario ?> </p>
                      </div>
                    </div>
                  </div>
                </section>
                <section>
                  <div class="row">                                              
                  </div>
                </section>
              </div>
              <!-- edit-profile -->
              <div id="edit-profile" class="tab-pane">
                <section class="panel">                                          
                  <div class="panel-body bio-graph-info">
                    <h1> Editar Información</h1>
                    <form class="form-horizontal" role="form" id="edit-perfil">
                      <div class="form-group">
                        <label class="col-lg-2 control-label">Tipo</label>
                        <div class="col-lg-4">
                          <?php
                            if ($usuario[0]->Tipo_Usuario == 1) {
                              echo '<select class="form-control" name="Tipo_Usuario" id="T-usu" readonly>';
                              echo '<option value="1" id="T-usu1">Administrador</option>';
                            } else if ($usuario[0]->Tipo_Usuario == 2) {
                              echo '<select class="form-control" name="Tipo_Usuario" id="T-usu" readonly>';
                              echo '<option value="2" id="T-usu2">Doctor</option>';
                            } else{
                              if ($_SESSION["user"]->Tipo_Usuario == 1) {
                                echo '<select class="form-control" name="Tipo_Usuario" id="T-usu">';
                                echo '<option value="1" id="T-usu1">Administrador</option>';
                                echo '<option value="2" id="T-usu2">Doctor</option>';
                                echo '<option value="3" id="T-usu3" selected>Cliente</option>';
                              } else {
                                echo '<select class="form-control" name="Tipo_Usuario" id="T-usu" readonly>';
                                echo '<option value="3" id="T-usu3">Cliente</option>';
                              }
                            }
                            echo '</select>';
                          ?>
                        </div>
                        <label class="col-lg-2 control-label">Foto</label>
                        <div class="col-lg-4">
                          <!-- <label for="exampleInputFile">File input</label> -->
                          <input type="file" id="UploadFile" name="Imagen_Usuario">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-lg-2 control-label">Nombre</label>
                        <div class="col-lg-4">
                          <input type="text" class="form-control" name="Nombre_Usuario" id="f-name" placeholder=" " value="<?= $usuario[0]->Nombre_Usuario ?>">
                        </div>
                        <label class="col-lg-2 control-label">Apellidos</label>
                        <div class="col-lg-4">
                          <input type="text" class="form-control" name="Nombre2_Usuario" id="l-name" placeholder=" " value="<?= $usuario[0]->Nombre2_Usuario ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-lg-2 control-label">Género</label>
                        <div class="col-lg-4">
                          <select class="form-control" name="Genero_Usuario" id="Gn">
                            <?php
                              //Opcion para vacío
                              if ($usuario[0]->Genero_Usuario == '') {
                                echo '<option value="" id="Gn-Vacio" selected></option>';
                              } else {
                                echo '<option value="" id="Gn-Vacio"></option>';
                              }
                              //Opcion para Femenino
                              if ($usuario[0]->Genero_Usuario == 'Femenino') {
                                echo '<option value="Femenino" id="Gn-F" selected>Femenino</option>';
                              } else {
                                echo '<option value="Femenino" id="Gn-F">Femenino</option>';
                              }
                              //Opcion para Masculino
                              if ($usuario[0]->Genero_Usuario == 'Masculino') {
                                echo '<option value="Masculino" id="Ec-Vacio" selected>Masculino</option>';
                              } else {
                                echo '<option value="Masculino" id="Ec-Vacio">Masculino</option>';
                              }
                            ?>
                          </select>
                        </div>
                        <label class="col-lg-2 control-label">Edad</label>
                        <div class="col-lg-4">
                          <input type="text" class="form-control" name="Edad_Usuario" id="edad" placeholder=" " value="<?= $usuario[0]->Edad_Usuario ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-lg-2 control-label">Estado civil</label>
                        <div class="col-lg-4">
                          <select class="form-control" name="EstadoCivil_Usuario" id="Ec">
                            <?php
                              //Opcion para vacío
                              if ($usuario[0]->EstadoCivil_Usuario == '') {
                                echo '<option value="" id="Ec-Vacio" selected></option>';
                              } else {
                                echo '<option value="" id="Ec-Vacio"></option>';
                              }
                              //Opcion para Soltero(a)
                              if ($usuario[0]->EstadoCivil_Usuario == 'Soltero(a)') {
                                echo '<option value="Soltero(a)" id="Ec-S" selected>Soltero(a)</option>';
                              } else{
                                echo '<option value="Soltero(a)" id="Ec-S">Soltero(a)</option>';
                              }
                              //Opcion para Union Libre
                              if ($usuario[0]->EstadoCivil_Usuario == 'Union Libre') {
                                echo '<option value="Union Libre" id="Ec-UL" selected>Union Libre</option>';
                              } else{
                                echo '<option value="Union Libre" id="Ec-UL">Union Libre</option>';
                              }
                              //Opcion para Casado(a)
                              if ($usuario[0]->EstadoCivil_Usuario == 'Casado(a)') {
                                echo '<option value="Casado(a)" id="Ec-C" selected>Casado(a)</option>';
                              } else{
                                echo '<option value="Casado(a)" id="Ec-C">Casado(a)</option>';
                              }
                              //Opcion para Divorciado(a)
                              if ($usuario[0]->EstadoCivil_Usuario == 'Divorciado(a)') {
                                echo '<option value="Divorciado(a)" id="Ec-D" selected>Divorciado(a)</option>';
                              } else{
                                echo '<option value="Divorciado(a)" id="Ec-D">Divorciado(a)</option>';
                              }
                              //Opcion para Viudo(a)
                              if ($usuario[0]->EstadoCivil_Usuario == 'Viudo(a)') {
                                echo '<option value="Viudo(a)" id="Ec-V" selected>Viudo(a)</option>';
                              } else{
                                echo '<option value="Viudo(a)" id="Ec-V">Viudo(a)</option>';
                              }
                            ?>
                          </select>
                        </div>
                        <label class="col-lg-2 control-label">Ocupación</label>
                        <div class="col-lg-4">
                          <input type="text" class="form-control" name="Ocupacion_Usuario" id="ocupacion" placeholder=" " value="<?= $usuario[0]->Ocupacion_Usuario ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-lg-2 control-label">Dirección</label>
                        <div class="col-lg-4">
                          <input type="text" class="form-control" name="Direccion_Usuario" id="dir" placeholder=" " value="<?= $usuario[0]->Direccion_Usuario ?>">
                        </div>
                        <label class="col-lg-2 control-label">Colonia</label>
                        <div class="col-lg-4">
                          <input type="text" class="form-control" name="Colonia_Usuario" id="colonia" placeholder=" " value="<?= $usuario[0]->Colonia_Usuario ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-lg-2 control-label">Ciudad</label>
                        <div class="col-lg-4">
                          <input type="text" class="form-control" name="Ciudad_Usuario" id="ciudad" placeholder=" " value="<?= $usuario[0]->Ciudad_Usuario ?>">
                        </div>
                        <label class="col-lg-2 control-label">Estado</label>
                        <div class="col-lg-4">
                          <input type="text" class="form-control" name="Region_Usuario" id="estado" placeholder=" " value="<?= $usuario[0]->Region_Usuario ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-lg-2 control-label">Código Postal</label>
                        <div class="col-lg-4">
                          <input type="text" class="form-control" name="CodigoPostal_Usuario" id="cp" placeholder=" " value="<?= $usuario[0]->CodigoPostal_Usuario ?>">
                        </div>
                        <label class="col-lg-2 control-label">Fecha de nacimiento</label>
                        <div class="col-lg-4">
                          <input type="date" class="form-control" name="FechaNacim_Usuario" id="fech-nacim" placeholder=" " value="<?= $usuario[0]->FechaNacim_Usuario ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-lg-2 control-label">Teléfono 1</label>
                        <div class="col-lg-4">
                          <input type="tel" class="form-control" name="Telefono1_Usuario" id="tel1" placeholder=" " value="<?= $usuario[0]->Telefono1_Usuario ?>">
                        </div>
                        <label class="col-lg-2 control-label">Teléfono 2</label>
                        <div class="col-lg-4">
                          <input type="tel" class="form-control" name="Telefono2_Usuario" id="tel2" placeholder=" " value="<?= $usuario[0]->Telefono2_Usuario ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-lg-2 control-label">Email</label>
                        <div class="col-lg-4">
                          <input type="email" class="form-control" name="Email_Usuario" id="mail" placeholder="nombre@hotmail.com " value="<?= $usuario[0]->Email_Usuario ?>">
                        </div>
                        <label class="col-lg-2 control-label">Contraseña</label>
                        <div class="col-lg-4">
                          <input type="password" class="form-control" id="pass" value="<?= $usuario[0]->Pass_Usuario ?>" readonly>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-lg-2 control-label">Nueva Contraseña</label>
                        <div class="col-lg-4">
                          <input type="password" class="form-control" id="n-pass" placeholder="****" pattern=".{8,}" title="Ingrese 8 caracteres como mínimo.">
                        </div>
                        <label class="col-lg-2 control-label">Confirmar Nueva Contraseña</label>
                        <div class="col-lg-4">
                          <input type="password" class="form-control" id="cn-pass" placeholder="****" pattern=".{8,}" title="Ingrese 8 caracteres como mínimo." name="Pass_Usuario">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10" align="right">
                          <button type="submit" class="btn btn-primary" id="guardar">Guardar</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </section>
              </div>
              <div id="expediente-profile" class="tab-pane">
                <?php 

                // var_dump($usuario[0]->Expediente_Usuario) ;
                if ( $usuario[0]->Expediente_Usuario == "0" ) {

                ?>

                <section class="panel">
                  <div class="panel-body bio-graph-info">
                    Este usuario no cuenta con expediente. 
                    <button 
                      class="btn btn-default"
                      id="addExp"
                      >
                        Agregar Expediente
                    </button>
                  </div>
                </section>

                <?php

                } else {

                ?>
                <section class="panel">

                  <!-- Inicio de interrogatorio -->
                  <div class="panel-body bio-graph-info">
                    <h1>Expediente # <?=$usuario[0]->Expediente_Usuario?></h1>
                  </div>
                  <div class="panel-body bio-graph-info">
                    <form class="form-horizontal" role="form" id="interrogatorio-perfil">
                      <h1> Interrogatorio</h1>
                      <div class="form-group">
                        <label class="col-lg-3 control-label">Alergias a medicamentos</label>
                        <div class="col-lg-3" align="left">
                          <label class="control-label col-lg-4">
                            <?php if ($AlergiaMedica == 1) {
                              echo '<input name="Aler_med" id="Aler_med1" value="1" type="radio" checked/>';
                            } else{
                              echo '<input name="Aler_med" id="Aler_med1" value="1" type="radio"/>';
                            }?>
                             Sí
                          </label>
                          <label class="control-label col-lg-4">
                            <?php if ($AlergiaMedica == 1) {
                              echo '<input name="Aler_med" id="Aler_med2" value="0" type="radio" />';
                            } else{
                              echo '<input name="Aler_med" id="Aler_med2" value="0" type="radio" checked/>';
                            }
                            ?>
                             No
                          </label>
                        </div>
                        <label class="col-lg-3 control-label">¿Cual?</label>
                        <div class="col-lg-3">
                          <?php
                            if ($AlergiaMedica == 1) {
                          ?>
                            <input type="text" class="form-control" name="Aler_medD" id="Aler_medD" value="<?= $DescripAlerMed ?>">
                          <?php
                            } else{
                              echo '<input type="text" class="form-control" name="Aler_medD" id="Aler_medD" value="">';
                            }
                          ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-lg-3 control-label">¿Lo han anestesiado anteriormente?</label>
                        <div class="col-lg-3">
                          <label class="control-label col-lg-4">
                          <?php 
                            if ($Anestesia == 1) {
                              echo '<input name="Anestesia" id="Anestesia1" value="1" type="radio" checked/>';
                            } else{
                              echo '<input name="Anestesia" id="Anestesia1" value="1" type="radio" />';
                            }
                          ?>
                             Sí
                          </label>
                          <label class="control-label col-lg-4">
                          <?php
                            if ($Anestesia == 1) {
                              echo '<input name="Anestesia" id="Anestesia2" value="0" type="radio" />';
                            } else{
                              echo '<input name="Anestesia" id="Anestesia2" value="0" type="radio" checked/>';
                            }
                          ?>
                             No
                          </label>
                        </div>
                        <label class="col-lg-3 control-label">¿Tuvo problemas cuando se la aplicaron?</label>
                        <div class="col-lg-3">
                          <label class="control-label col-lg-4">
                          <?php
                            if ($ProblemAnestesia == 1) {
                              echo '<input name="Anest_prob" id="Anest_prob1" value="1" type="radio" checked/>';
                            } else{
                              echo '<input name="Anest_prob" id="Anest_prob1" value="1" type="radio" />';
                            }
                          ?>
                             Sí
                          </label>
                          <label class="control-label col-lg-4">
                          <?php
                            if ($ProblemAnestesia == 1) {
                              echo '<input name="Anest_prob" id="Anest_prob2" value="0" type="radio" />';
                            } else{
                              echo '<input name="Anest_prob" id="Anest_prob2" value="0" type="radio" checked/>';
                            }
                          ?>
                             No
                          </label>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-lg-3 control-label">¿Qué le sucedió?</label>
                        <div class="col-lg-3">
                        <?php
                          if ($ProblemAnestesia == 1) {
                            echo '<input type="text" class="form-control" name="Anest_probD" id="Anest_probD" value="'. $DescripAnes .'">';
                          } else{
                            echo '<input type="text" class="form-control" name="Anest_probD" id="Anest_probD" value="">';
                          }
                        ?>
                        </div>
                        <label class="col-lg-3 control-label">¿Esta tomando algún medicamento?</label>
                        <div class="col-lg-3">
                          <label class="control-label col-lg-4">
                          <?php
                            if ($TomaMedicamento == 1) {
                              echo '<input name="Toma_med" id="Toma_med1" value="1" type="radio" checked/>';
                            } else{
                              echo '<input name="Toma_med" id="Toma_med1" value="1" type="radio" />';
                            }
                          ?>
                             Sí
                          </label>
                          <label class="control-label col-lg-4">
                          <?php
                            if ($TomaMedicamento == 1) {
                              echo '<input name="Toma_med" id="Toma_med2" value="0" type="radio" />';
                            } else{
                              echo '<input name="Toma_med" id="Toma_med2" value="0" type="radio" checked/>';
                            }
                          ?>
                             No
                          </label>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-lg-3 control-label">¿Cuales?</label>
                        <div class="col-lg-3">
                          <?php
                            if ($TomaMedicamento == 1) {
                              echo '<input type="text" class="form-control" name="Toma_medD" id="Toma_medD" value="'. $DescripMed .'">';
                            } else{
                              echo '<input type="text" class="form-control" name="Toma_medD" id="Toma_medD" value="">';
                            }
                          ?>
                          
                        </div>
                        <label class="col-lg-3 control-label">¿Adicciones?</label>
                        <div class="col-lg-3">
                          <label class="control-label col-lg-4">
                          <?php
                            if ($Adicciones == 1) {
                              echo '<input name="Adiccion" id="Adiccion1" value="1" type="radio" checked/>';
                            } else{
                              echo '<input name="Adiccion" id="Adiccion1" value="1" type="radio" />';
                            }
                          ?>
                             Sí
                          </label>
                          <label class="control-label col-lg-4">
                          <?php
                            if ($Adicciones == 1) {
                              echo '<input name="Adiccion" id="Adiccion2" value="0" type="radio" />';
                            } else{
                              echo '<input name="Adiccion" id="Adiccion2" value="0" type="radio" checked/>';
                            }
                          ?>
                             No
                          </label>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-lg-3 control-label">¿Cuales?</label>
                        <div class="col-lg-3">
                          <?php
                            if ($Adicciones == 1) {
                              echo '<input type="text" class="form-control" name="AdiccionD" id="AdiccionD" value="'. $CualesAdicc .'">';
                            } else{
                              echo '<input type="text" class="form-control" name="AdiccionD" id="AdiccionD" value="">';
                            }
                          ?>
                        </div>
                        <label class="col-lg-3 control-label">¿Fuma?</label>
                        <div class="col-lg-3">
                          <label class="control-label col-lg-4">
                            <?php
                              if ($Fumador == 1) {
                                echo '<input name="Fuma" id="Fuma1" value="1" type="radio" checked/>';
                              } else{
                                echo '<input name="Fuma" id="Fuma1" value="1" type="radio" />';
                              }
                            ?>
                             Sí
                          </label>
                          <label class="control-label col-lg-4">
                            <?php
                              if ($Fumador == 1) {
                                echo '<input name="Fuma" id="Fuma2" value="0" type="radio" />';
                              } else{
                                echo '<input name="Fuma" id="Fuma2" value="0" type="radio" checked/>';
                              }
                            ?>
                             No
                          </label>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-lg-3 control-label">¿Ingiere bebidas alcoholicas?</label>
                        <div class="col-lg-3">
                          <label class="control-label col-lg-4">
                            <?php
                              if ($BebAlcohol == 1) {
                                echo '<input name="Alcohol" id="Alcohol1" value="1" type="radio" checked/>';
                              } else{
                                echo '<input name="Alcohol" id="Alcohol1" value="1" type="radio" />';
                              }
                            ?>
                             Sí
                          </label>
                          <label class="control-label col-lg-4">
                            <?php
                              if ($BebAlcohol == 1) {
                                echo '<input name="Alcohol" id="Alcohol2" value="0" type="radio" />';
                              } else{
                                echo '<input name="Alcohol" id="Alcohol2" value="0" type="radio" checked/>';
                              }
                            ?>
                             No
                          </label>
                        </div>
                        <label class="col-lg-3 control-label">¿Embarazo?</label>
                        <div class="col-lg-3">
                          <label class="control-label col-lg-4">
                            <?php
                              if ($Embarazada == 1) {
                                echo '<input name="Embarazo" id="Embarazo1" value="1" type="radio" checked/>';
                              } else{
                                echo '<input name="Embarazo" id="Embarazo1" value="1" type="radio" />';
                              }
                            ?>
                             Sí
                          </label>
                          <label class="control-label col-lg-4">
                            <?php
                              if ($Embarazada == 1) {
                                echo '<input name="Embarazo" id="Embarazo2" value="0" type="radio" />';
                              } else{
                                echo '<input name="Embarazo" id="Embarazo2" value="0" type="radio" checked/>';
                              }
                            ?>
                             No
                          </label>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-lg-3 control-label">¿Meses de gestación?</label>
                        <div class="col-lg-3">
                          <?php
                            if ($Embarazada == 1) {
                              echo '<input type="number" class="form-control" name="EmbarazoM" id="EmbarazoM" min="0" max="9" value="'. $MesesGesta .'">';
                            } else{
                              echo '<input type="number" class="form-control" name="EmbarazoM" id="EmbarazoM" min="0" max="9" value="">';
                            }
                          ?>
                        </div>
                        <label class="col-lg-3 control-label">¿Tiene problemas con su periodo?</label>
                        <div class="col-lg-3">
                          <label class="control-label col-lg-4">
                            <?php
                              if ($ProblCPeriodo == 1) {
                                echo '<input name="Periodo_prob" id="Periodo_prob1" value="1" type="radio" checked/>';
                              } else{
                                echo '<input name="Periodo_prob" id="Periodo_prob1" value="1" type="radio" />';
                              }
                            ?>
                             Sí
                          </label>
                          <label class="control-label col-lg-4">
                            <?php
                              if ($ProblCPeriodo == 1) {
                                echo '<input name="Periodo_prob" id="Periodo_prob2" value="0" type="radio" />';
                              } else{
                                echo '<input name="Periodo_prob" id="Periodo_prob2" value="0" type="radio" checked/>';
                              }
                            ?>
                             No
                          </label>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-lg-3 control-label">¿Alguna otra enfermedad?</label>
                        <div class="col-lg-3">
                          <?php
                            if ($OtraEnferm != "") {
                              echo '<input type="text" class="form-control" name="Otra_enfD" id="Otra_enfD" value="'. $OtraEnferm .'">';
                            } else{
                              echo '<input type="text" class="form-control" name="Otra_enfD" id="Otra_enfD" value="">';
                            }
                          ?>
                        </div>
                        <div class="col-lg-6">
                          <!-- Boton de submit -->
                          <div class="col-lg-2"></div>
                          <div class="form-group col-lg-8" align="center">
                            <input type="submit" class="btn btn-primary btn-block" id="Guardar-int" value="Guardar">
                          </div>
                          <div class="col-lg-2"></div>
                          <!-- Fin de boton de submit -->
                        </div>
                      </div>
                    </form>
                  </div>
                  <!-- Fin de interrogatorio -->

                  <!-- Inicia Antecedentes patológicos -->
                  <div class="panel-body bio-graph-info">
                    <form class="form-horizontal" role="form" id="AntPat-perfil">
                      <h1> Antecedentes Patológicos</h1>
                      <div class="form-group">
                        <label class="col-lg-3 control-label">Hipertensión</label>
                        <div class="col-lg-3">
                          <label class="control-label col-lg-4">
                            <input name="Hipertension" id="Hipertension1" value="1" type="radio" /> Sí
                          </label>
                          <label class="control-label col-lg-4">
                            <input name="Hipertension" id="Hipertension2" value="0" type="radio" checked/> No
                          </label>
                        </div>
                        <label class="col-lg-3 control-label">Cardiopatías</label>
                        <div class="col-lg-3">
                          <label class="control-label col-lg-4">
                            <input name="Cardiopatias" id="Cardiopatias1" value="1" type="radio" /> Sí
                          </label>
                          <label class="control-label col-lg-4">
                            <input name="Cardiopatias" id="Cardiopatias2" value="0" type="radio" checked/> No
                          </label>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-lg-3 control-label">Diabetes</label>
                        <div class="col-lg-3">
                          <label class="control-label col-lg-4">
                            <input name="Diabetes" id="Diabetes1" value="1" type="radio" /> Sí
                          </label>
                          <label class="control-label col-lg-4">
                            <input name="Diabetes" id="Diabetes2" value="0" type="radio" checked/> No
                          </label>
                        </div>
                        <label class="col-lg-3 control-label">Fiebre reumática (Epilepsia)</label>
                        <div class="col-lg-3">
                          <label class="control-label col-lg-4">
                            <input name="Epilepsia" id="Epilepsia1" value="1" type="radio" /> Sí
                          </label>
                          <label class="control-label col-lg-4">
                            <input name="Epilepsia" id="Epilepsia2" value="0" type="radio" checked/> No
                          </label>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-lg-3 control-label">Parotiditis</label>
                        <div class="col-lg-3">
                          <label class="control-label col-lg-4">
                            <input name="Parotiditis" id="Parotiditis1" value="1" type="radio" /> Sí
                          </label>
                          <label class="control-label col-lg-4">
                            <input name="Parotiditis" id="Parotiditis2" value="0" type="radio" checked/> No
                          </label>
                        </div>
                        <label class="col-lg-3 control-label">Hepatitis</label>
                        <div class="col-lg-3">
                          <label class="control-label col-lg-4">
                            <input name="Hepatitis" id="Hepatitis1" value="1" type="radio" /> Sí
                          </label>
                          <label class="control-label col-lg-4">
                            <input name="Hepatitis" id="Hepatitis2" value="0" type="radio" checked/> No
                          </label>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-lg-3 control-label">Asma</label>
                        <div class="col-lg-3">
                          <label class="control-label col-lg-4">
                            <input name="Asma" id="Asma1" value="1" type="radio" /> Sí
                          </label>
                          <label class="control-label col-lg-4">
                            <input name="Asma" id="Asma2" value="0" type="radio" checked/> No
                          </label>
                        </div>
                        <label class="col-lg-3 control-label">Cáncer</label>
                        <div class="col-lg-3">
                          <label class="control-label col-lg-4">
                            <input name="Cancer" id="Cancer1" value="1" type="radio" /> Sí
                          </label>
                          <label class="control-label col-lg-4">
                            <input name="Cancer" id="Cancer2" value="0" type="radio" checked/> No
                          </label>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-lg-3 control-label">E.T.S.</label>
                        <div class="col-lg-3">
                          <label class="control-label col-lg-4">
                            <input name="ETS" id="ETS1" value="1" type="radio" /> Sí
                          </label>
                          <label class="control-label col-lg-4">
                            <input name="ETS" id="ETS2" value="0" type="radio" checked/> No
                          </label>
                        </div>
                        <label class="col-lg-3 control-label">Tuberculosis</label>
                        <div class="col-lg-3">
                          <label class="control-label col-lg-4">
                            <input name="Tuberculosis" id="Tuberculosis1" value="1" type="radio" /> Sí
                          </label>
                          <label class="control-label col-lg-4">
                            <input name="Tuberculosis" id="Tuberculosis2" value="0" type="radio" checked/> No
                          </label>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-lg-3 control-label">Artritis</label>
                        <div class="col-lg-3">
                          <label class="control-label col-lg-4">
                            <input name="Artritis" id="Artritis1" value="1" type="radio" /> Sí
                          </label>
                          <label class="control-label col-lg-4">
                            <input name="Artritis" id="Artritis2" value="0" type="radio" checked/> No
                          </label>
                        </div>
                        <label class="col-lg-3 control-label">Alergias</label>
                        <div class="col-lg-3">
                          <label class="control-label col-lg-4">
                            <input name="Alergias" id="Alergias1" value="1" type="radio" /> Sí
                          </label>
                          <label class="control-label col-lg-4">
                            <input name="Alergias" id="Alergias2" value="0" type="radio" checked/> No
                          </label>
                        </div>
                      </div>
                      <!-- Boton de submit -->
                      <div class="col-lg-6"></div>
                      <div class="col-lg-6">
                        <div class="col-lg-2"></div>
                        <div class="form-group col-lg-8" align="center">
                          <input type="submit" class="btn btn-primary btn-block" id="Guardar-antpat" value="Guardar">
                        </div>
                        <div class="col-lg-2"></div>
                      </div>
                      <!-- Fin de boton de submit -->
                    </form>
                  </div>
                  <!-- Fin Antecedentes patológicos -->

                  <!-- Inicia Antecedentes patológicos familiares -->
                  <div class="panel-body bio-graph-info">
                    <form class="form-horizontal" role="form" id="AntPatFam-perfil">
                      <h1> Antecedentes Patológicos Familiares</h1>
                      <div class="form-group">
                        <label class="col-lg-2 control-label"><center>Padecimiento</center></label>
                        <label class="col-lg-2 control-label"><center>Madre</center></label>
                        <label class="col-lg-2 control-label"><center>Padre</center></label>
                        <label class="col-lg-2 control-label"><center>Abuelos Maternos</center></label>
                        <label class="col-lg-2 control-label"><center>Abuelos Paternos</center></label>
                        <label class="col-lg-2 control-label"><center>Otros</center></label>
                      </div>
                      <div class="form-group">
                        <label class="col-lg-2 control-label">Diabetes</label>
                        <input class="col-lg-2" name="DiabetesFM" id="DiabetesFM" type="checkbox" />
                        <input class="col-lg-2" name="DiabetesFP" id="DiabetesFP" type="checkbox" />
                        <input class="col-lg-2" name="DiabetesFAM" id="DiabetesFAM" type="checkbox" />
                        <input class="col-lg-2" name="DiabetesFAP" id="DiabetesFAP" type="checkbox" />
                        <div class="col-lg-2">
                          <input type="text" class="form-control" name="DiabetesFO" id="DiabetesFO">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-lg-2 control-label">Hipertensión Arterial</label>
                        <input class="col-lg-2" name="HipertensionFM" id="HipertensionFM" type="checkbox" />
                        <input class="col-lg-2" name="HipertensionFP" id="HipertensionFP" type="checkbox" />
                        <input class="col-lg-2" name="HipertensionFAM" id="HipertensionFAM" type="checkbox" />
                        <input class="col-lg-2" name="HipertensionFAP" id="HipertensionFAP" type="checkbox" />
                        <div class="col-lg-2">
                          <input type="text" class="form-control" name="HipertensionFO" id="HipertensionFO">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-lg-2 control-label">Cardiopatías</label>
                        <input class="col-lg-2" name="CardiopatiaFM" id="CardiopatiaFM" type="checkbox" />
                        <input class="col-lg-2" name="CardiopatiaFP" id="CardiopatiaFP" type="checkbox" />
                        <input class="col-lg-2" name="CardiopatiaFAM" id="CardiopatiaFAM" type="checkbox" />
                        <input class="col-lg-2" name="CardiopatiaFAP" id="CardiopatiaFAP" type="checkbox" />
                        <div class="col-lg-2">
                          <input type="text" class="form-control" name="CardiopatiaFO" id="CardiopatiaFO">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-lg-2 control-label">Neoplasias</label>
                        <input class="col-lg-2" name="NeoplasiasFM" id="NeoplasiasFM" type="checkbox" />
                        <input class="col-lg-2" name="NeoplasiasFP" id="NeoplasiasFP" type="checkbox" />
                        <input class="col-lg-2" name="NeoplasiasFAM" id="NeoplasiasFAM" type="checkbox" />
                        <input class="col-lg-2" name="NeoplasiasFAP" id="NeoplasiasFAP" type="checkbox" />
                        <div class="col-lg-2">
                          <input type="text" class="form-control" name="NeoplasiasFO" id="NeoplasiasFO">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-lg-2 control-label">Epilepsias</label>
                        <input class="col-lg-2" name="EpilepsiasFM" id="EpilepsiasFM" type="checkbox" />
                        <input class="col-lg-2" name="EpilepsiasFP" id="EpilepsiasFP" type="checkbox" />
                        <input class="col-lg-2" name="EpilepsiasFAM" id="EpilepsiasFAM" type="checkbox" />
                        <input class="col-lg-2" name="EpilepsiasFAP" id="EpilepsiasFAP" type="checkbox" />
                        <div class="col-lg-2">
                          <input type="text" class="form-control" name="EpilepsiasFO" id="EpilepsiasFO">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-lg-2 control-label">Malformaciones</label>
                        <input class="col-lg-2" name="MalformacionesFM" id="MalformacionesFM" type="checkbox" />
                        <input class="col-lg-2" name="MalformacionesFP" id="MalformacionesFP" type="checkbox" />
                        <input class="col-lg-2" name="MalformacionesFAM" id="MalformacionesFAM" type="checkbox" />
                        <input class="col-lg-2" name="MalformacionesFAP" id="MalformacionesFAP" type="checkbox" />
                        <div class="col-lg-2">
                          <input type="text" class="form-control" name="MalformacionesFO" id="MalformacionesFO">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-lg-2 control-label">SIDA</label>
                        <input class="col-lg-2" name="SidaFM" id="SidaFM" type="checkbox" />
                        <input class="col-lg-2" name="SidaFP" id="SidaFP" type="checkbox" />
                        <input class="col-lg-2" name="SidaFAM" id="SidaFAM" type="checkbox" />
                        <input class="col-lg-2" name="SidaFAP" id="SidaFAP" type="checkbox" />
                        <div class="col-lg-2">
                          <input type="text" class="form-control" name="DiabetesFO" id="DiabetesFO">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-lg-2 control-label">Enfermedades Renales</label>
                        <input class="col-lg-2" name="RenalesFM" id="RenalesFM" type="checkbox" />
                        <input class="col-lg-2" name="RenalesFP" id="RenalesFP" type="checkbox" />
                        <input class="col-lg-2" name="RenalesFAM" id="RenalesFAM" type="checkbox" />
                        <input class="col-lg-2" name="RenalesFAP" id="RenalesFAP" type="checkbox" />
                        <div class="col-lg-2">
                          <input type="text" class="form-control" name="DiabetesFO" id="DiabetesFO">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-lg-2 control-label">Artritis</label>
                        <input class="col-lg-2" name="ArtritisFM" id="ArtritisFM" type="checkbox" />
                        <input class="col-lg-2" name="ArtritisFP" id="ArtritisFP" type="checkbox" />
                        <input class="col-lg-2" name="ArtritisFAM" id="ArtritisFAM" type="checkbox" />
                        <input class="col-lg-2" name="ArtritisFAP" id="ArtritisFAP" type="checkbox" />
                        <div class="col-lg-2">
                          <input type="text" class="form-control" name="ArtritisFO" id="ArtritisFO">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-lg-2 control-label">Hepatitis</label>
                        <input class="col-lg-2" name="HepatitisFM" id="HepatitisFM" type="checkbox" />
                        <input class="col-lg-2" name="HepatitisFP" id="HepatitisFP" type="checkbox" />
                        <input class="col-lg-2" name="HepatitisFAM" id="HepatitisFAM" type="checkbox" />
                        <input class="col-lg-2" name="HepatitisFAP" id="HepatitisFAP" type="checkbox" />
                        <div class="col-lg-2">
                          <input type="text" class="form-control" name="HepatitisFO" id="HepatitisFO">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-lg-2 control-label">Otros</label>
                        <div class="col-lg-10">
                          <input type="text" class="form-control" name="OtrosF" id="OtrosF">
                        </div>
                      </div>
                      <!-- Boton de submit -->
                      <div class="col-lg-6"></div>
                      <div class="col-lg-6">
                        <div class="col-lg-2"></div>
                        <div class="form-group col-lg-8" align="center">
                          <input type="submit" class="btn btn-primary btn-block" id="Guardar-antpatfam" value="Guardar">
                        </div>
                        <div class="col-lg-2"></div>
                      </div>
                      <!-- Fin de boton de submit -->
                    </form>
                  </div>
                  <!-- Fin Antecedentes patológicos familiares -->
                  
                </section>
                <?php 
                  }
                ?>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
    <!-- page end-->
              
    <script>
        // Script para verificar la nueva contraseña y su confirmación al momento de ejecutar el botón "Guardar"
        $(document).ready(function(){
            $("form#edit-perfil").submit(function(event){

                $('#guardar').prop('disabled', true);

                if($("#n-pass").val() != $("#cn-pass").val()){
                    bootbox.alert("Su nueva contraeña no coincide con su confirmación. Verifique y vuelva a intentarlo");
                    event.preventDefault();
                    return false;
                }

                event.preventDefault();
                var fromobj = $( "form#edit-perfil" ).serializeArray();
                var idPerfil = "<?= $idperfil ?>";
                var jsonDatos = {idPerfil:idPerfil,datos:fromobj};
                console.log(fromobj,jsonDatos);

                var img = $("#UploadFile")[0].files[0];
                console.log(img);

                if ( $("#UploadFile")[0].files.length == 0 ){
                  var aFileParts = [''];
                  img = new Blob(aFileParts, {type : 'image/jpeg'}); // the blob
                  console.log( "vacio" );
                }
                else
                  console.log("si hay");

                var r = new FileReader();
                r.onloadend = function(){
                  //console.log(r.result);
                  fromobj.push( { name : "Imagen_Usuario", value : window.btoa(r.result) } );
                  console.log(fromobj);

                  var jqxhr = $.ajax( {
                    url: "<?= base_url() ?>Perfil/actualiza_perfil?idPerfil="+idPerfil,
                    method: "POST",
                    data: fromobj,
                    dataType: "json"
                    //cache: false,
                    //processData: false
                    //contentType: false
                  })
                    .done(function(res) {
                      console.log(res);
                      if (!res.err){
                          var msg = "Los datos se actualizaron con éxito";
                          var calb = function(){location.reload()};
                      } else {
                          var msg = "No se actualizaron los datos";
                          var calb = function(){$('#guardar').prop('disabled', false);};
                      }
                      bootbox.alert( {
                          message: msg,
                          callback: calb
                      } );
                    })
                    .fail(function(a,b,c) {
                      console.log(a,b,c);
                      bootbox.alert( "Ocurrió un error al actualizar tus datos" );
                      $('#guardar').prop('disabled', false);
                    })
                    .always(function() {
                      //bootbox.alert( "complete" );
                    });
                   
                  // Perform other work here ...
                   
                  // Set another completion function for the request above
                  jqxhr.always(function() {
                    //bootbox.alert( "second complete" );
                  });

                };
                r.readAsBinaryString(img);
                //console.log(r.readAsBinaryString(img));
                return false;
            });

            $("#addExp").click(function(){

                $('#addExp').prop('disabled', true);

                bootbox.confirm({ 
                  size: "small",
                  message: "Desea agregar un nuevo expediente a este usuario?", 
                  callback: function(result){
                    // console.log(result);
                    if ( !result ) {
                      $('#addExp').prop('disabled', false);
                    }
                    else {
                      var idPerfil = "<?= $idperfil ?>";
                      var jqxhr = $.ajax( {
                        url: "<?= base_url() ?>Perfil/add_Expediente?idPerfil="+idPerfil,
                        method: "GET",
                        dataType: "json"
                      })
                        .done(function(res) {
                          console.log(res);
                          if (!res.error){
                              var msg = "Los datos se actualizaron con éxito";
                              var calb = function(){location.reload(true)};
                          } else {
                              var msg = "No se actualizaron los datos";
                              var calb = function(){$('#addExp').prop('disabled', false);};
                          }
                          bootbox.alert( {
                              message: msg,
                              callback: calb
                          } );
                        })
                        .fail(function(a,b,c) {
                          console.log(a,b,c);
                          bootbox.alert( "Ocurrió un error al agregar el expediente" );
                          $('#addExp').prop('disabled', false);
                        })
                        
                    }
                  }
                })

            });
        });
    </script>
  </section>
</section>
<!--main content end-->