
<!--main content start-->
<section id="main-content">
    <section class="wrapper">            
        <!--overview start-->
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header"><i class="fa fa-laptop"></i> Citas</h3>
                <ol class="breadcrumb">
                    <li><i class="fa fa-home"></i><a href="<?= base_url() ?>Admin/Inicio">Inicio</a></li>
                    <li><i class="fa fa-calendar"></i>Citas</li>                          
                </ol>
            </div>
        </div>

        <!--Calendario de citas-->
        <div class="row">
            <div class="col-lg-12 portlets">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2><strong>Calendario de citas</strong></h2>
                    </div><br><br><br>
                    <div class="panel-body">
                        <!-- Below line produces calendar. I am using FullCalendar plugin. -->
                        <div id="calendario"></div>
                    </div>
                </div> 
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 portlets">
                <!--Project Activity start-->
                <section class="panel">
                    <div class="panel-body progress-panel">
                        <div class="row">
                            <div class="col-lg-8 task-progress pull-left">
                                <h1>Todas las citas</h1>                                  
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="tab-cli" class="table table-striped table-advance table-hover">
                            <thead>
                                <tr>
                                    <th><i class="icon_calendar"></i> Fecha</th>
                                    <th><i class="icon_profile"></i> Cliente</th>
                                    <th><i class="icon_clock_alt"></i> De las</th>
                                    <th><i class="icon_clock_alt"></i> A las</th>
                                    <th><i class="icon_profile"></i> Doctor</th>
                                    <th><i class="icon_info_alt"></i> Confirmada</th>
                                    <th><i class="icon_toolbox_alt"></i> Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- Lista de usuarios -->
                                <?php 
                                  foreach ($citasConsulta as $fila) {
                                ?>
                                <tr>
                                    <td>
                                    <?php
                                      setlocale(LC_ALL,"es_MX");
                                      echo utf8_encode(strftime("%A %d de %B del %Y",strtotime($fila->HraIni_Citas)));
                                    ?>
                                    </td>
                                    <td><?= $fila->ID_Usuario ?></td>
                                    <td><?= substr($fila->HraIni_Citas,11,5) ?></td>
                                    <td><?= substr($fila->HraFin_Citas,11,5) ?></td>
                                    <td><?= $fila->ID_UsuDent ?></td>
                                    <td><?= $fila->ID_EstatusCita ?></td>
                                    <td>
                                        <center><div class="btn-group">
                                            <a class="btn btn-danger" id="cancelaCita" name="cancelaCita" data-toggle="Tooltip" title="Cancelar Cita" onclick="Cancelar(<?= $fila->ID_Cita ?>)"><i class="icon_trash_alt"></i></a>
                                        </div></center>
                                    </td>
                                  </tr>
                                <?php
                                  }
                                ?>
                             </tbody>
                        </table>
                    </div>
                </section>
            </div>
        </div>
        <!--Fin de calendario de citas-->

        <!-- Formulario Modal Agregar citas -->
        <div id="addModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Agregar Cita</h4>
                    </div>
                    <form class="form-horizontal" role="form" id="addEvent">
                        <div class="modal-body">
                            <!-- Clientes -->
                            <div class="form-group">
                                <label class="control-label col-lg-1">Cliente</label>
                                <div class="col-lg-11">                               
                                    <select class="form-control" id="ID_Usuario" name="ID_Usuario">
                                        <option value="">- Elija un cliente -</option>
                                        <?php
                                            $cli = $this->Primero->get_Clientes();
                                            foreach ($cli->result() as $fila) {
                                        ?>
                                        <option value="<?= $fila->ID_Usuario ?>"><?= $fila->Nombre_Usuario . ' ' . $fila->Nombre2_Usuario ?></option>
                                        <?php
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <!-- Fecha y hora de inicio -->
                            <div class="form-group">
                                <label class="control-label col-lg-1" for="title">De</label>
                                <div class="col-lg-11">
                                    <input type="datetime-local" class="form-control" name="HraIni_Citas" id="HraIni_Citas">
                                </div>
                            </div>
                            <!-- Fecha y hora de finalización -->
                            <div class="form-group">
                                <label class="control-label col-lg-1" for="title">A</label>
                                <div class="col-lg-11">
                                    <input type="datetime-local" class="form-control" name="HraFin_Citas" id="HraFin_Citas">
                                </div>
                            </div>
                            <!-- Recurrencia -->
                            <div class="form-group">
                                <label class="control-label col-lg-1" for="title">Recurrencia</label>
                                <br>
                                <div class="radios">
                                    <label class="label-radio col-lg-3">
                                        <input name="Recurrente_Cita" id="recur1" value="1" type="radio" checked /> Una vez
                                    </label>
                                    <label class="label-radio col-lg-3">
                                        <input name="Recurrente_Cita" id="recur2" value="2" type="radio" /> Semanal
                                    </label>
                                    <label class="label-radio col-lg-3">
                                        <input name="Recurrente_Cita" id="recur3" value="3" type="radio" /> Quincenal
                                    </label>
                                    <label class="label-radio col-lg-3">
                                        <input name="Recurrente_Cita" id="recur4" value="4" type="radio" /> Mensual
                                    </label>
                                </div>
                            </div>
                            <div class="form-group" id="form_cantidad">
                                <label class="control-label col-lg-1" for="title">No. Citas</label>
                                <div class="col-lg-11">
                                    <input type="number" class="form-control" name="Cant_Citas" id="Cant_Citas">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="submit" class="btn btn-primary" id="Aceptar" value="Aceptar">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Fin Formulario Modal Agregar Citas -->

        <!-- Formulario Modal Modificar citas -->
        <div id="modModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Modificar Cita</h4>
                    </div>
                    <form class="form-horizontal" role="form" id="modEvent">
                        <div class="modal-body">
                            <!-- ID Cita -->                              
                            <input type="hidden" class="form-control" name="ID_CitaM" id="ID_CitaM">
                            <!-- Clientes -->
                            <div class="form-group">
                                <label class="control-label col-lg-1">Cliente</label>
                                <div class="col-lg-11">                               
                                    <input type="text" class="form-control" name="UsuM" id="UsuM" readonly>
                                </div>
                            </div>
                            <!-- Fecha y hora de inicio -->
                            <div class="form-group">
                                <label class="control-label col-lg-1" for="title">De</label>
                                <div class="col-lg-11">
                                    <input type="datetime-local" class="form-control" name="HraIniM_Citas" id="HraIniM_Citas">
                                </div>
                            </div>
                            <!-- Fecha y hora de finalización -->
                            <div class="form-group">
                                <label class="control-label col-lg-1" for="title">A</label>
                                <div class="col-lg-11">
                                    <input type="datetime-local" class="form-control" name="HraFinM_Citas" id="HraFinM_Citas">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-lg-1" for="title">Estatus</label>
                                <div class="col-lg-11">                               
                                    <select class="form-control" id="ID_EstatusCitas" name="ID_EstatusCitas">
                                        <option value="">Pendiente</option>
                                        <option value="Confirmado">Confirmado</option>
                                        <option value="Cancelado">Cancelado</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="submit" class="btn btn-primary" id="Guardar" value="Guardar">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Fin Formulario Modal Agregar Citas -->
    </div> 
</div>
<!--Project Activity end-->

<!-- project team & activity end -->
</section>

    <!-- Full calendar -->
    <link rel="stylesheet" href="<?= base_url() ?>CosasNiceAdmin/assets/fullcalendar-3.4.0/fullcalendar.min.css" />
    <script src='<?= base_url() ?>CosasNiceAdmin/assets/fullcalendar-3.4.0/lib/moment.min.js'></script>
    <script src='<?= base_url() ?>CosasNiceAdmin/assets/fullcalendar-3.4.0/fullcalendar.js'></script>
    <script src='<?= base_url() ?>CosasNiceAdmin/assets/fullcalendar-3.4.0/locale/es.js'></script>
    <script src='<?= base_url() ?>CosasNiceAdmin/assets/fullcalendar-3.4.0/gcal.js'></script>
    <!--script for this page only-->

    <script src="<?= base_url() ?>CosasNiceAdmin/js/tablaDinamica.js"></script>
    <script>
    /*
      //knob
      $(function() {
        $(".knob").knob({
          'draw' : function () { 
            $(this.i).val(this.cv + '%')
          }
        })
      });
    */
    /* ----------- Calendario ----------- */
    /*
    $(document).ready(function() {
    
        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();
        
        $('#calendario').fullCalendar({
            header: { 
                left: 'prev,next,nextYear today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            editable: true,
            events: [
                {
                    title: 'Todo el día',
                    start: new Date(y, m, 1)
                },
                {
                    title: 'Evento Largo',
                    start: new Date(y, m, d-5),
                    end: new Date(y, m, d-2)
                },
                {
                    id: 999,
                    title: 'Evento Recurrente',
                    start: new Date(y, m, d-3, 16, 0),
                    allDay: false
                },
                {
                    id: 999,
                    title: 'Evento Recurrente',
                    start: new Date(y, m, d+4, 16, 0),
                    allDay: false
                },
                {
                    title: 'Junta',
                    start: new Date(y, m, d, 10, 30),
                    allDay: false
                },
                {
                    title: 'Almuerzo',
                    start: new Date(y, m, d, 12, 0),
                    end: new Date(y, m, d, 14, 0),
                    allDay: false
                },
                {
                    title: 'Cumpleaños',
                    start: new Date(y, m, d+1, 19, 0),
                    end: new Date(y, m, d+1, 22, 30),
                    allDay: false
                },
                {
                    title: 'Click a Google',
                    start: new Date(y, m, 28),
                    end: new Date(y, m, 29),
                    url: 'http://google.com/'
                }
            ]
        });
        
    });
    */

    /* ----------- Calendario ----------- */
    $(document).ready(function() {
    
        var date1 = new Date();
        
        $('#calendario').fullCalendar({
            locale: 'es',
            header: { 
                left: 'prev,next,nextYear today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            navLinks: true,
            eventLimit: true,
            editable: true,
            allDaySlot: false,
            firstDay: 1,

            events: "<?= base_url() ?>Citas/obtenerCitas",

            dayClick: function(date, jsEvent, view) {
                date_last_clicked = $(this);
                $(this).css('background-color', '#bed7f3');
                
                var fecha = date.toISOString().split('-');
                var hora = date1.toTimeString().split(':');

                //-------- Colocando la fecha inicial --------//
                document.getElementById("HraIni_Citas").value = fecha[0].toString() + '-' + fecha[1].toString() + '-' + fecha[2].toString() + 'T' + hora[0].toString() + ':' + hora[1].toString();

                //-------- Colocando la fecha final --------//
                document.getElementById("HraFin_Citas").value = fecha[0].toString() + '-' + fecha[1].toString() + '-' + fecha[2].toString() + 'T' + hora[0].toString() + ':' + hora[1].toString();
                $('#addModal').modal();
            },
            eventDrop: function(event, delta, revertFunc) {

                //bootbox.alert(event.title + " was dropped on " + event.start.format());

                if (confirm("Desea mover la cita de " + event.title + " para el " + event.start.format("DD MMM YYYY HH:mm") + " ?")) {
                    modificarCita(event,revertFunc);
                } else{
                    revertFunc();
                }

            },
            eventResize: function(event, delta, revertFunc, jsEvent, ui, view) {
                if (confirm("Desea actualizar la cita de " + event.title + " de " + event.start.format("HH:mm") + " a "+ event.end.format("HH:mm") +" ?")) {
                    modificarCita(event,revertFunc);
                } else{
                    revertFunc();
                }
            },
            eventClick: function(event, jsEvent, view) {

                $(this).css('background-color', '#bed7f3');

                
                $('#ID_CitaM').val(event.id);
                $('#UsuM').val(event.title);

                //Colocando horas del evento
                document.getElementById("HraIniM_Citas").value = event.start.format('YYYY')+'-'+event.start.format('MM')+'-'+event.start.format('DD')+'T'+event.start.format('HH')+':'+event.start.format('mm');
                document.getElementById("HraFinM_Citas").value = event.end.format('YYYY')+'-'+event.end.format('MM')+'-'+event.end.format('DD')+'T'+event.end.format('HH')+':'+event.end.format('mm');
                $('#modModal').modal();
            }
        });

        // Funcionamiento del Form para agregar la cita
        $("form#addEvent").submit(function(event){

            $('#Aceptar').prop('disabled', true);

            event.preventDefault();

            if ($('#ID_Usuario').val() == "") {
                bootbox.alert('Debe elegir un cliente para poder continuar');
                $('#Aceptar').prop('disabled', false);
            } else{
                var fromobj = $( "form#addEvent" ).serializeArray();
                console.log(fromobj);

                //console.log(r.result);
                var jqxhr = $.ajax( {
                    url: "<?= base_url() ?>Citas/add_event",
                    method: "POST",
                    data: fromobj,
                    dataType: "html"
                    //cache: false,
                    //processData: false
                    //contentType: false
                })
                .done(function(res) {
                    console.log(res);
                    if (!res.err){
                        var msg = "Cita agregada";
                        var calb = function(){location.reload()};
                    } else {
                        var msg = "No se logró agregar la cita";
                        var calb = function(){$('#Aceptar').prop('disabled', false);};
                    }
                    bootbox.alert( {
                        message: msg,
                        callback: calb
                    } );
                })
                .fail(function(a,b,c) {
                    console.log(a,b,c);
                    bootbox.alert( "Ocurrió un error al agregar la cita" );
                    $('#Aceptar').prop('disabled', false);
                })
                .always(function(e) {
                    console.log(e);
                });
                   
                // Perform other work here ...
                   
                // Set another completion function for the request above
                jqxhr.always(function() {
                    //bootbox.alert( "second complete" );
                });
                //console.log(r.readAsBinaryString(img));
                return false;
            }
        });

        if ($('input[name=Recurrente_Cita]:checked', '#addEvent').val() != 1) {
                $('#form_cantidad').show();
        } else{
            $('#form_cantidad').hide();
        }

        $('#addEvent input').on('change', function() {
           //alert($('input[name=Recurrente_Cita]:checked', '#addEvent').val()); 
           if ($('input[name=Recurrente_Cita]:checked', '#addEvent').val() != 1) {
                $('#form_cantidad').show();
           } else{
                $('#form_cantidad').hide();
                $('#Cant_Citas').val("");
           }
        });

         // Funcionamiento del Form para modificar la cita
        $("form#modEvent").submit(function(event){

            $('#Guardar').prop('disabled', true);

            event.preventDefault();
            
            var fromobj = $( "form#modEvent" ).serializeArray();
            console.log(fromobj);

            //console.log(r.result);
            var jqxhr = $.ajax( {
                url: "<?= base_url() ?>Citas/modModal_event",
                method: "POST",
                data: fromobj,
                dataType: "html"
                //cache: false,
                //processData: false
                //contentType: false
            })
            .done(function(res) {
                console.log(res);
                if (!res.err){
                    var msg = "Cita modificada";
                    var calb = function(){location.reload()};
                } else {
                    var msg = "No se logró modificar la cita";
                    var calb = function(){$('#Guardar').prop('disabled', false);};
                }
                bootbox.alert( {
                    message: msg,
                    callback: calb
                } );
            })
            .fail(function(a,b,c) {
                console.log(a,b,c);
                bootbox.alert( "Ocurrió un error al modificar la cita" );
                $('#Guardar').prop('disabled', false);
            })
            .always(function(e) {
                console.log(e);
            });
               
            // Perform other work here ...
               
            // Set another completion function for the request above
            jqxhr.always(function() {
                //bootbox.alert( "second complete" );
            });
            //console.log(r.readAsBinaryString(img));
            return false;
        });
        
    });

    //Funcion para modificar la cita desde la manipulación del evento dentro del calendario
    function modificarCita(datosCita,revertFunc){
        
        console.log(datosCita);
        //return;

        var jsonCita = {'ID_Cita':datosCita.id,
                        'HraIni_Citas':datosCita.start.format(),
                        'HraFin_Citas':datosCita.end.format()
                        };

        var jqxhr = $.ajax( {
            url: "<?= base_url() ?>Citas/mod_event",
            method: "POST",
            data: jsonCita,
            dataType: "html"
        })
        .done(function(res) {
            console.log(res);
        })
        .fail(function(a,b,c) {
            console.log(a,b,c);
            bootbox.alert( "Ocurrió un error al cambiar la cita" );
            revertFunc();
        })
        .always(function() {
        });
    };

    //Función para cancelar una cita
    function Cancelar(id){

      if (confirm("¿Esta seguro de cancelar esta cita?") == true){
        var jqxhr = $.post("<?= base_url() ?>Citas/cancela_Cita", {idCit:id}, function(data) {
        },"text")
        .done(function(data) {
          console.log(data);

          bootbox.alert({
            message: "Cita cancelada!",
            callback: function(){ location.reload() }
          });
        })
        .fail(function(a,b,c) {
          console.log(a,b,c);
          bootbox.alert( "Ocurrió un error al cancelar la cita. Inténtelo de nuevo." );
        })
      } 
    }

  </script>