<!--main content start-->
      <section id="main-content">
          <section class="wrapper">            
              <!--overview start-->
              <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header"><i class="fa fa-laptop"></i> Principal</h3>
                    <ol class="breadcrumb">
                        <li><i class="fa fa-home"></i><a href="#">Inicio</a></li>
                        <li><i class="fa fa-laptop"></i>Principal</li>                          
                    </ol>
                </div>
            </div>
              
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <div class="info-box blue-bg">
                        <i class="fa fa-cloud-download"></i>
                        <div class="count">6.674</div>
                        <div class="title">Download</div>                       
                    </div><!--/.info-box-->         
                </div><!--/.col-->
                
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <div class="info-box brown-bg">
                        <i class="fa fa-shopping-cart"></i>
                        <div class="count">7.538</div>
                        <div class="title">Purchased</div>                      
                    </div><!--/.info-box-->         
                </div><!--/.col-->  
                
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <div class="info-box dark-bg">
                        <i class="fa fa-thumbs-o-up"></i>
                        <div class="count">4.362</div>
                        <div class="title">Order</div>                      
                    </div><!--/.info-box-->         
                </div><!--/.col-->
                
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <div class="info-box green-bg">
                        <i class="fa fa-cubes"></i>
                        <div class="count">1.426</div>
                        <div class="title">Stock</div>                      
                    </div><!--/.info-box-->         
                </div><!--/.col-->
                
            </div><!--/.row-->
          

        <div class="row">
            <div class="col-md-6 portlets">
            <div class="panel panel-default">
                <div class="panel-heading">
                  <h2><strong>Calendario</strong></h2>
                <div class="panel-actions">
                    <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a> 
                    <a href="#" class="wclose"><i class="fa fa-times"></i></a>
                </div>  
                 
                </div><br><br><br>
                <div class="panel-body">
                  <!-- Widget content -->
                  
                <!-- Below line produces calendar. I am using FullCalendar plugin. -->
                <div id="calendario"></div>
                </div>
              </div> 
               
            </div>
                 <div class="col-md-6 portlets">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <div class="pull-left">Quick Post</div>
                  <div class="widget-icons pull-right">
                    <a href="#" class="wminimize"><i class="fa fa-chevron-up"></i></a> 
                    <a href="#" class="wclose"><i class="fa fa-times"></i></a>
                  </div>  
                  <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                  <div class="padd">                    
                      <div class="form quick-post">
                      <!-- Edit profile form (not working)-->
                      <form class="form-horizontal">
                          <!-- Title -->
                          <div class="form-group">
                            <label class="control-label col-lg-2" for="title">Title</label>
                            <div class="col-lg-10"> 
                              <input type="text" class="form-control" id="title">
                            </div>
                          </div>   
                          <!-- Content -->
                          <div class="form-group">
                            <label class="control-label col-lg-2" for="content">Content</label>
                            <div class="col-lg-10">
                              <textarea class="form-control" id="content"></textarea>
                            </div>
                          </div>                           
                          <!-- Cateogry -->
                          <div class="form-group">
                            <label class="control-label col-lg-2">Category</label>
                            <div class="col-lg-10">                               
                                <select class="form-control">
                                  <option value="">- Choose Cateogry -</option>
                                  <option value="1">General</option>
                                  <option value="2">News</option>
                                  <option value="3">Media</option>
                                  <option value="4">Funny</option>
                                </select>  
                            </div>
                          </div>              
                          <!-- Tags -->
                          <div class="form-group">
                            <label class="control-label col-lg-2" for="tags">Tags</label>
                            <div class="col-lg-10">
                              <input type="text" class="form-control" id="tags">
                            </div>
                          </div>
                          
                          <!-- Buttons -->
                          <div class="form-group">
                             <!-- Buttons -->
                             <div class="col-lg-offset-2 col-lg-9">
                                <button type="submit" class="btn btn-primary">Publish</button>
                                <button type="submit" class="btn btn-danger">Save Draft</button>
                                <button type="reset" class="btn btn-default">Reset</button>
                             </div>
                          </div>
                      </form>
                    </div>
                  </div>
                  <div class="widget-foot">
                    <!-- Footer goes here -->
                  </div>
                </div>
              </div>
              
            </div>
                        
          </div> 
              <!-- project team & activity end -->
              
          </section>

    <!-- Full calendar -->
    <script src='<?= base_url() ?>CosasNiceAdmin/assets/fullcalendar-3.4.0/lib/moment.min.js'></script>
    <script src='<?= base_url() ?>CosasNiceAdmin/assets/fullcalendar-3.4.0/fullcalendar.js'></script>
    <script src='<?= base_url() ?>CosasNiceAdmin/assets/fullcalendar-3.4.0/locale/es.js'></script>

    <!--script for this page only-->
    <script>

    /* ----------- Calendario ----------- */

    $(document).ready(function() {
    
        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();
        
        $('#calendario').fullCalendar({
            locale: 'es',
            firstDay: 0,
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            editable: true,
            events: [
                {
                    title: 'Todo el día',
                    start: new Date(y, m, 1)
                },
                {
                    title: 'Evento Largo',
                    start: new Date(y, m, d-5),
                    end: new Date(y, m, d-2),
                    color: "green"
                },
                {
                    id: 999,
                    title: 'Evento Recurrente',
                    start: new Date(y, m, d-3, 16, 0),
                    allDay: false,
                    color: "orange"
                },
                {
                    id: 999,
                    title: 'Evento Recurrente',
                    start: new Date(y, m, d+4, 16, 0),
                    allDay: false
                },
                {
                    title: 'Junta',
                    start: new Date(y, m, d, 10, 30),
                    allDay: false,
                    color: "red"
                },
                {
                    title: 'Almuerzo',
                    start: new Date(y, m, d, 12, 0),
                    end: new Date(y, m, d, 14, 0),
                    allDay: false
                },
                {
                    title: 'Cumpleaños',
                    start: new Date(y, m, d+1, 19, 0),
                    end: new Date(y, m, d+1, 22, 30),
                    allDay: false
                },
                {
                    title: 'Click a Google',
                    start: new Date(y, m, 28),
                    end: new Date(y, m, 29),
                    url: 'http://google.com/'
                }
            ]
        });
        
    });

  </script>