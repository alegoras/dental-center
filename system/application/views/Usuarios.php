      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
		  <div class="row">
				<div class="col-lg-12">
					<h3 class="page-header"><i class="icon_profile"></i> Usuarios</h3>
					<ol class="breadcrumb">
						<li><i class="fa fa-home"></i><a href="<?= base_url() . 'Admin/Inicio' ?>">Inicio</a></li>
						<li><i class="fa fa-list"></i>Usuarios</li>
					</ol>
				</div>
			</div>
              <!-- page start-->
              <div class="row">
                <div class="col-lg-12">
                  <div class="btn-group">
                    <a class="btn btn-primary" href="<?= base_url() ?>Perfil/Ver"><span class="icon_plus_alt2"></span> Nuevo</a>
                    <br><p>
                    <br><p>
                  </div>
                </div>
              </div>
              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              Lista de usuarios
                          </header>
                          <div class="table-responsive">
                            <table id="tab-cli" class="table table-striped table-advance table-hover">
                             <thead>
                              <tr>
                                 <th><i class="icon_profile"></i> Nombre</th>
                                 <th><i class="icon_calendar"></i> Registro</th>
                                 <th><i class="icon_mail_alt"></i> Email</th>
                                 <th><i class="icon_pin_alt"></i> Ciudad</th>
                                 <th><i class="icon_mobile"></i> Teléfono</th>
                                 <th><i class="icon_info_alt"></i> Activo</th>
                                 <th><i class="icon_cogs"></i> Acciones</th>
                              </tr>
                             </thead>
                             <tbody>
                                <!-- Lista de usuarios -->
                                <?php 
                                  foreach ($consulta->result() as $fila) {
                                ?>
                                    <tr>
                                       <td><?= $fila->Nombre_Usuario . ' ' . $fila->Nombre2_Usuario ?></td>
                                       <td><?= $fila->FechaNacim_Usuario ?></td>
                                       <td><?= $fila->Email_Usuario ?></td>
                                       <td><?= $fila->Ciudad_Usuario ?></td>
                                       <td><?= $fila->Telefono1_Usuario ?></td>
                                       <td>
                                          <center><input type="checkbox" class="activar" name="act-cli<?= $fila->ID_Usuario ?>" id="act-cli" value="<?= $fila->ID_Usuario ?>"
                                            <?php
                                              if ($fila->Activo_Usuario == 1) {
                                                echo 'checked';
                                              }
                                            ?>
                                          /></center>
                                       </td>
                                       <td>
                                        <center>
                                          <div class="btn-group">
                                              <!-- <a class="btn btn-primary" href="#"><i class="icon_plus_alt2"></i></a> -->
                                                <a class="btn btn-success" data-toggle="Tooltip" title="Editar" href="<?= base_url() ?>Perfil/Ver/<?= $fila->ID_Usuario ?>"><i class="icon_pencil"></i></a>
                                              <!-- <a class="btn btn-danger" data-toggle="Tooltip" title="Eliminar" href="#"><i class="icon_trash"></i></a> -->
                                          </div>
                                        </center>
                                       </td>
                                    </tr>
                                <?php
                                  }
                                ?>
                             </tbody>
                          </table>
                        </div>
                      </section>
                  </div>
              </div>
              <!-- page end-->
          </section>
      <!--main content end-->

      <!-- javascripts -->
    <!--custom checkbox & radio-->
    <script type="text/javascript" src="<?= base_url() ?>CosasNiceAdmin/js/ga.js"></script>
    <!--custom switch-->
    <script src="<?= base_url() ?>CosasNiceAdmin/js/bootstrap-switch.js"></script>
    <!--custom tagsinput-->
    <script src="<?= base_url() ?>CosasNiceAdmin/js/jquery.tagsinput.js"></script>
    <!-- bootstrap-wysiwyg -->
    <script src="<?= base_url() ?>CosasNiceAdmin/js/jquery.hotkeys.js"></script>
    <script src="<?= base_url() ?>CosasNiceAdmin/js/bootstrap-wysiwyg.js"></script>
    <script src="<?= base_url() ?>CosasNiceAdmin/js/bootstrap-wysiwyg-custom.js"></script>
    <!-- ck editor -->
    <script type="text/javascript" src="<?= base_url() ?>CosasNiceAdmin/assets/ckeditor/ckeditor.js"></script>
    
    <!-- Este script es escencial para que funcione el SWITCH -->
    <!-- custom form component script for this page-->
    <script src="<?= base_url() ?>CosasNiceAdmin/js/form-component.js"></script>
    <script src="<?= base_url() ?>CosasNiceAdmin/js/tablaDinamica.js"></script>
    <script>
    
    $(document).ready(function () {
        $('.activar').click(function(event){
          var idSwitchUsu = $(this).val()
          var esteElemento = $(this)
          var esteChBx = $(this).prop('checked')
          var jqxhr = $.post("<?= base_url() ?>Perfil/cambiar_activo", {idUsu:idSwitchUsu, chk:esteChBx}, function(data) {
          },"json")
            .done(function(data) {
              console.log(data);

              bootbox.alert({
                message: "Estatus cambiado",
                callback: function(){ location.reload() }
              });
            })
            .fail(function(a,b,c) {
              console.log(a,b,c);
              bootbox.alert( "Ocurrió un error al cambiar el estatus. Inténtelo de nuevo." );
              esteElemento.prop( "checked", !esteChBx);
            })
        });
    });
    
    
    </script>